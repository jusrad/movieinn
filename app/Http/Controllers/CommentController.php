<?php


namespace App\Http\Controllers;


use App\Filmo_komentaras;
use App\TVshowComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function PostComment(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'turinys'=>'required',
            'film_id_post'=>'required'
        ], [
            'turinys.required' => 'Comment is empty'
        ]);

        if($validate->fails())
        {
            return Redirect::back()->withErrors($validate);
        }
        else
        {
            $comment = new Filmo_komentaras();
            $comment->turinys = $request['turinys'];
            $comment->fk_Filmas = $request['film_id_post'];
            $comment->parent_id = $request['parent_id'];
            $comment->rating = 0.0;
            date_default_timezone_set('Europe/Vilnius');
            $comment->sukurimo_data = date("Y-m-d");
            $comment->naudotojas = session()->get('username');
            $comment->fk_Naudotojas=Auth::id();
            $comment->save();
        }

        return back();
    }

    public function PostTVshowComment(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'turinys'=>'required',
            'film_id_post'=>'required'
        ], [
            'turinys.required' => 'Comment is empty'
        ]);

        if($validate->fails())
        {
            return Redirect::back()->withErrors($validate);
        }
        else
        {
            $comment = new TVshowComment();
            $comment->content = $request['turinys'];
            $comment->fk_com_TVshow = $request['film_id_post'];
            $comment->parent_id = $request['parent_id'];
            $comment->rating = 0.0;
            date_default_timezone_set('Europe/Vilnius');
            $comment->create_date = date("Y-m-d");
            $comment->user = session()->get('username');
            $comment->fk_com_User=Auth::id();
            $comment->save();
        }

        return back();
    }

    public function editComment(Request $request, $id)
    {
        $comment = Filmo_komentaras::where('id_FKomentaras', $id)->first();

        $validate = Validator::make($request->all(), [
            'turinys'=>'required',
            'film_id_post'=>'required'
        ], [
            'turinys.required' => 'Comment is empty'
        ]);

        if($validate->fails())
        {
            return Redirect::back()->withErrors($validate);
        }
        else
        {
            $comment->turinys = $request['turinys'];

            if (strpos($comment->turinys, '*EDITED*') === false) {
                $comment->turinys = $comment->turinys . "\n" . "\n" . '*EDITED*';
            }

            $comment->update();
        }

        return back();
    }

    public function editTVshowComment(Request $request, $id)
    {
        $comment = TVshowComment::where('id_TVshow_comment', $id)->first();

        $validate = Validator::make($request->all(), [
            'turinys'=>'required',
            'film_id_post'=>'required'
        ], [
            'turinys.required' => 'Comment is empty'
        ]);

        if($validate->fails())
        {
            return Redirect::back()->withErrors($validate);
        }
        else
        {
            $comment->content = $request['turinys'];
            $comment->update();
        }

        return back();
    }

    public function deleteComment($id)
    {
        $comment = Filmo_komentaras::where('id_FKomentaras', $id)->first();
        $replies = $comment->replies;
        if(count($replies) != 0)
        {
            $comment->is_deleted = true;
            if(Auth::user()->hasRole('administratorius') and Auth::id() !== $comment->fk_Naudotojas)
            {
                $comment->turinys = '*This comment was deleted by the administrator*';
            }
            else
            {
                $comment->turinys = '*This comment was deleted by the user*';
            }
            $comment->save();
        }
        else
        {
            $parent_id = $comment->parent_id;
            Filmo_komentaras::where('id_FKomentaras', $id)->delete();

            if($parent_id !== 0)
            {
                $parent = Filmo_komentaras::where('id_FKomentaras', $parent_id)->first();
                if($parent->is_deleted and count($parent->replies) === 0)
                {
                    $parent->delete();
                }
            }
        }

        return redirect()->back()->with('success', 'Comment deleted');
    }

    public function deleteTVShowComment($id)
    {
        $comment = TVshowComment::where('id_TVshow_comment', $id)->first();
        $replies = $comment->replies;
        if(count($replies) != 0)
        {
            $comment->is_deleted = true;
            if(Auth::user()->hasRole('administratorius') and Auth::id() !== $comment->fk_com_User)
            {
                $comment->content = '*This comment was deleted by the administrator*';
            }
            else
            {
                $comment->content = '*This comment was deleted by the user*';
            }
            $comment->save();
        }
        else
        {
            $parent_id = $comment->parent_id;
            TVshowComment::where('id_TVshow_comment', $id)->delete();
            $parent = TVshowComment::where('id_TVshow_comment', $parent_id)->first();
            if($parent->is_deleted and count($parent->replies) === 0)
            {
                $parent->delete();
            }
        }

        return redirect()->back()->with('success', 'Comment deleted');
    }
}
