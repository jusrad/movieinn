<?php

namespace App\Http\Controllers;

use App\Filmo_komentaras;
use App\LikeMovie;
use App\LikeTVshow;
use App\Mail\ReleasesShipped;
use App\PeopleNews;
use App\Person;
use App\PersonMark;
use App\TvShow;
use App\TVshowComment;
use App\User;
use App\Filmas;
use App\UserList;
use App\UserListItem;
use App\Watchlist;
use Exception;
use Illuminate\Support\Facades\Storage;
use Image;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware( 'verified');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(!session()->has('username') && Auth::check())
        {
            $name = User::where('id', Auth::id())->select('name')->first()['name'];
            session(['username' => $name]);
        }

        $allFilms = Filmas::all()->map->only(['id_Filmas', 'pavadinimas', 'isleidimo_data', 'vertinimas', 'photolink'])->toArray();

        if(count($allFilms) === 0)
        {
            $filmchunk = array_chunk($allFilms, 4);
            $placeholderfilm = new Filmas();
            array_push($filmchunk, $placeholderfilm);
            return view('film.film_list', ["allFilms" => $filmchunk]);
        }

        for($i = 0; $i < count($allFilms); $i++)
        {
            $tmp = Carbon::createFromFormat('Y-m-d', $allFilms[$i]['isleidimo_data'])->year;
            $allFilms[$i]['isleidimo_data'] = $tmp;
        }

        $filmchunk = array_chunk($allFilms, 4);
        $placeholderfilm = new Filmas();
        for ($x = count($filmchunk[count($filmchunk) - 1]); $x < 4; $x++) {
            array_push($filmchunk[count($filmchunk) - 1], $placeholderfilm);
        }

        return view('film.film_list', ["allFilms" => $filmchunk]);
    }

    public function getuserinfo()
    {
        if(Auth::check())
        {
            $user = User::where('id', Auth::user()->id)->first();
            return view('user.edit_user', compact('user'));
        }
        else
        {
            return Redirect::to('/home/');
        }
    }

    public function editUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'picture' => 'mimes:jpeg,jpg,png'
        ]);

        if ($validator->fails())
        {
            $request->flash();
            return Redirect::back()->withErrors($validator);
        }
        else
        {
            $data = $request->all();
            $user = User::where('id', Auth::id())->first();
            $user->update(['name' => $data['name']]);
            $user->update(['mark' => $data['userTag']]);
            $user->update(['description' => $data['description']]);

            $avatar = $request->file('picture');
            $filename = time().'.'.$avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(200, 200)->save(storage_path('app/public/'.$filename));

            //TODO remove old file, not deleting
            $old_image = $user->avatar;
            Storage::disk('local')->delete('avatars/'.$old_image);
            $user->update(['avatar' => $filename]);
        }

        return Redirect::to('/home/editForm')->with('success', 'User data updated');
    }

    public function addUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'picture' => 'image|mimes:jpg,png'
        ]);

        if ($validator->fails())
        {
            $request->flash();
            return Redirect::back()->withErrors($validator);
        }
        else
        {
            $data = $request->all();
            $user = User::where('id', Auth::id())->first();
            $user->update(['mark' => $data['userTag']]);
            $user->update(['description' => $data['description']]);

            $avatar = $request->file('picture');
            $filename = time().'.'.$avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(200, 200)->save(storage_path('avatars/'.$filename));

            //TODO remove old file, not deleting
            $old_image = $user->avatar;
            Storage::disk('local')->delete('avatars/'.$old_image);
            $user->update(['avatar' => $filename]);
        }

        return Redirect::to('/home/editForm')->with('success', 'User data updated');
    }

    public function getUserDataForm()
    {
        if(Auth::check())
        {
            return view('user.user_form');
        }
        else
        {
            return back();
        }
    }

    public function getuserlist()
    {
        if(Auth::check() and Auth::user()->hasRole('administratorius'))
        {
            $users = User::all();
            return view('user.user_list', compact('users'));
        }
        else
        {
            return Redirect::to('/home/');
        }
    }

    public function deleteuser($id)
    {
        if(Auth::check() and Auth::user()->hasRole('administratorius'))
        {
            $made_tvshow_comments = TVshowComment::all()->where('fk_com_User', $id);
            foreach ($made_tvshow_comments as $comment)
            {
                (new CommentController)->deleteTVShowComment($comment['id_TVshow_comment']);
            }

            $made_movie_comments = Filmo_komentaras::all()->where('fk_Naudotojas', $id);
            foreach ($made_movie_comments as $comment)
            {
                (new CommentController)->deleteComment($comment['id_FKomentaras']);
            }

            LikeMovie::where('fk_like_User', $id)->delete();
            LikeTVshow::where('fk_like_User', $id)->delete();
            Watchlist::where('fk_Watchlist_User', $id)->delete();
            PersonMark::where('fk_User', $id)->delete();
            $user_lists = UserList::all()->where('fk_userlist_user', $id);

            foreach ($user_lists as $list)
            {
                UserListItem::where('fk_userlistitem_list', $list['id_user_list'])->delete();
                $list->delete();
            }

            User::where('id', $id)->delete();
            return redirect()->back()->with('success', 'User account deleted');
        }
        else
        {
            return Redirect::to('/home/');
        }
    }

    public function addToWatchlist($id, $is_movie)
    {
        if(Auth::check())
        {
            if($is_movie === '0')
            {
                if(Watchlist::where('fk_Watchlist_Movie', $id)->where('fk_Watchlist_User', Auth::id())->count() === 0)
                {
                    $watchlist = new Watchlist();
                    $watchlist->fk_Watchlist_User = Auth::id();
                    $watchlist->fk_Watchlist_Movie = $id;
                    $watchlist->fk_Watchlist_TVshow = null;
                    $watchlist->save();
                }
            }
            elseif ($is_movie === '1')
            {
                if(Watchlist::where('fk_Watchlist_TVshow', $id)->where('fk_Watchlist_User', Auth::id())->count() === 0)
                {
                    $watchlist = new Watchlist();
                    $watchlist->fk_Watchlist_User = Auth::id();
                    $watchlist->fk_Watchlist_Movie = null;
                    $watchlist->fk_Watchlist_TVshow = $id;
                    $watchlist->save();
                }
            }

            if($is_movie === '0')
            {
                return Redirect::to('/home/film/' . $id);
            }
            elseif ($is_movie === '1')
            {
                return Redirect::to('/show/details/' . $id);
            }
        }
        else
        {
            return Redirect::to('/login/');
        }
    }

    public function removeFromWatchlist($id, $is_movie)
    {
        if(Auth::check())
        {
            if($is_movie === '0')
            {
                if(Watchlist::where('fk_Watchlist_Movie', $id)->where('fk_Watchlist_User', Auth::id())->count() === 1)
                {
                    Watchlist::where('fk_Watchlist_User', Auth::id())->where('fk_Watchlist_Movie', $id)->delete();
                }
            }
            elseif ($is_movie === '1')
            {
                if(Watchlist::where('fk_Watchlist_TVshow', $id)->where('fk_Watchlist_User', Auth::id())->count() === 1)
                {
                    Watchlist::where('fk_Watchlist_User', Auth::id())->where('fk_Watchlist_TVshow', $id)->delete();
                }
            }
        }

        return redirect()->back();
    }

    public function getWatchlist()
    {
        $movies = array();
        foreach (Watchlist::where('fk_Watchlist_User', Auth::id())->cursor() as $connection) {
            if($connection->fk_Watchlist_Movie === null)
            {
                $tvshow = TvShow::where('id_TVShow', $connection->fk_Watchlist_TVshow)->first();
                array_push($movies, $tvshow);
            }
            else if($connection->fk_Watchlist_TVshow === null)
            {
                $movie = Filmas::where('id_Filmas', $connection->fk_Watchlist_Movie)->first();
                array_push($movies, $movie);
            }
        }

        return view('user.watchlist', compact('movies'));
    }

    public function getCreateForm()
    {
        if(Auth::check())
        {
            return view('user.list_form');
        }
        else
        {
            return Redirect::to('/home/')->withErrors('You are not logged in');
        }
    }

    public function createList(Request $request)
    {
        if(Auth::check())
        {
            $validator = Validator::make($request->all(), [
                'listName' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
                'description' => 'required'
            ]);

            if ($validator->fails())
            {
                return Redirect::back()->withErrors($validator);
            }
            else
            {
                $list = new UserList();
                $list->name = $request->input('listName');
                $list->description = $request->input('description');
                $list->fk_userlist_user = Auth::id();
                $list->save();

                return Redirect::to('/home/lists')->with('success', 'List created');
            }
        }
        else
        {
            return Redirect::to('/home/lists')->withErrors('You are not logged in');
        }
    }

    public function sendMail()
    {
        $users = User::all();
        foreach ($users as $user)
        {
            if($user['email_verified_at'] !== null)
            {
                $marked = $user->marked_people;
                $id_array = array();
                foreach ($marked as $mark)
                    array_push($id_array, $mark['id_person']);

                if(count($marked) !== 0)
                {
                    $people = Person::all()->only($id_array);
                    Mail::to($user['email'])->queue(new ReleasesShipped($people, $user['name']));
                }
            }
        }

        return back()->with('success', 'Email sent');
    }

    public function getLists()
    {
        if(Auth::check())
        {
            $lists = UserList::where('fk_userlist_user', Auth::id())->get();
            return view('user.lists', compact('lists'));
        }
        else
        {
            return Redirect::to('/login/');
        }
    }

    public function getListItems($id)
    {
        if(Auth::check())
        {
            $list = UserList::where('id_user_list', $id)->where('fk_userlist_user', Auth::id())->first();
            $list_items = UserListItem::all()->where('fk_userlistitem_list', $list['id_user_list']);

            $items = array();
            foreach ($list_items as $item_id)
            {
                if($item_id['fk_userlistitem_movie'] === null)
                {
                    $tvshow = TvShow::where('id_TVShow', $item_id['fk_userlistitem_tvshow'])->first();
                    array_push($items, $tvshow);
                }
                else if($item_id['fk_userlistitem_tvshow'] === null)
                {
                    $movie = Filmas::where('id_Filmas', $item_id['fk_userlistitem_movie'])->first();
                    array_push($items, $movie);
                }
            }

            return view('user.list', compact('list', 'items'));
        }
        else
        {
            return Redirect::to('/login/');
        }
    }

    public function getListsToAdd($id, $add_what)
    {
        if(Auth::check())
        {
            $lists = UserList::all()->where('fk_userlist_user', Auth::id())->toArray();
            if($add_what === '1')
            {
                $in_list = new Collection();
                foreach ($lists as $list)
                {
                    $movie_in_list = UserListItem::where('fk_userlistitem_list', $list['id_user_list'])
                        ->where('fk_userlistitem_movie', $id)->first();

                    if($movie_in_list !== null)
                    {
                        $in_list->push($list);
                    }
                }
            }
            else
            {
                $in_list = new Collection();
                foreach ($lists as $list)
                {
                    $tvshow_in_list = UserListItem::where('fk_userlistitem_list', $list['id_user_list'])
                        ->where('fk_userlistitem_tvshow', $id)->first();

                    if($tvshow_in_list !== null)
                    {
                        $in_list->push($list);
                    }
                }
            }

            return view('user.list_add_form', compact('lists', 'in_list', 'id', 'add_what'));
        }
        else
        {
            return Redirect::to('/login/');
        }
    }

    public function addToLists(Request $request,$add_what, $id)
    {
        if(Auth::check())
        {
            $lists = UserList::all()->where('fk_userlist_user', Auth::id());

            if($add_what === '1')
            {
                foreach ($lists as $list)
                {
                    UserListItem::where('fk_userlistitem_list', $list['id_user_list'])
                        ->where('fk_userlistitem_movie', $id)->delete();
                }

                if($request->input('lists') !== null)
                {
                    foreach($request->input('lists') as $list_id)
                    {
                        $list_item = new UserListItem();
                        $list_item->fk_userlistitem_list = $list_id;
                        $list_item->fk_userlistitem_movie = $id;
                        $list_item->fk_userlistitem_tvshow = null;
                        $list_item->save();
                    }
                }

                return Redirect::to('/home/film/'.$id);
            }
            else
            {
                foreach ($lists as $list)
                {
                    UserListItem::where('fk_userlistitem_list', $list['id_user_list'])
                        ->where('fk_userlistitem_tvshow', $id)->delete();
                }

                if($request->input('lists') !== null)
                {
                    foreach($request->input('lists') as $list_id)
                    {
                        $list_item = new UserListItem();
                        $list_item->fk_userlistitem_list = $list_id;
                        $list_item->fk_userlistitem_tvshow = $id;
                        $list_item->fk_userlistitem_movie = null;
                        $list_item->save();
                    }
                }

                return Redirect::to('/show/details/'.$id);
            }
        }
        else
        {
            return Redirect::to('/login/');
        }
    }

    public function removeFromList($id_list, $type, $id)
    {
        if(Auth::check())
        {
            if($type === '1')
            {
                UserListItem::where('fk_userlistitem_list', $id_list)->where('fk_userlistitem_movie', $id)->delete();
            }
            else if($type === '2')
            {
                UserListItem::where('fk_userlistitem_list', $id_list)->where('fk_userlistitem_tvshow', $id)->delete();
            }

            return Redirect::back();
        }
        else
        {
            return Redirect::to('/login/');
        }
    }

    public function deleteUserList($id)
    {
        if(Auth::check())
        {
            UserListItem::where('fk_userlistitem_list', $id)->delete();
            UserList::where('id_user_list', $id)->delete();

            return back()->with('success', 'List deleted');
        }
        else
        {
            return Redirect::to('/login/');
        }
    }

    public function getAbout()
    {
        return view('main.about');
    }


    public function testReleaseGet()
    {
        $client = new \Tmdb\Client(new \Tmdb\ApiToken(FilmController::API_TOKEN));
        $people = Person::all()->map->only(['id_Person', 'known_for'])->toArray();

        foreach ($people as $person)
        {
            $tmdb_person_credits = $client->getPeopleApi()->getCombinedCredits($person['id_Person']);
            if($person['known_for'] == 'Acting')
            {
                $this->updateReleases($tmdb_person_credits['cast'], $person['id_Person']);
            }
            else
            {
                $this->updateReleases($tmdb_person_credits['crew'], $person['id_Person']);
            }
        }

        return back()->with('success', 'Releases updated');
    }

    public function updateReleases($array, $id)
    {
        foreach ($array as $cast)
        {
            $date = '';
            $name = '';
            if($cast['media_type'] === 'tv')
            {
                if(array_key_exists('first_air_date', $cast))
                    $date = $cast['first_air_date'];
                else
                    $date = null;
                $name = $cast['name'];
            }
            else if($cast['media_type'] === 'movie')
            {
                if(array_key_exists('release_date', $cast))
                    $date = $cast['release_date'];
                else
                    $date = null;
                $name = $cast['title'];
            }

            if($date == null or $date == '' or strtotime($date) > strtotime(date("Y-m-d")))
            {
                $old_releases = PeopleNews::where('id_person_releases', $cast['id'])->first();
                if($old_releases === null)
                {
                    $new_releases = new PeopleNews();
                    $new_releases->id_person_releases = $cast['id'];
                    $new_releases->name = $name;

                    if($date == '')
                        $new_releases->release_date = null;
                    else
                        $new_releases->release_date = $date;

                    $new_releases->type = $cast['media_type'];
                    $new_releases->fk_releases_person = $id;
                    try{
                        $new_releases->save();
                    }
                    catch(Exception $e){
                        dd($old_releases, $new_releases);
                    }
                }
                else
                {
                    $old_releases->update(['name' => $name]);

                    if($date == '')
                        $old_releases->update(['release_date' => null]);
                    else
                        $old_releases->update(['release_date' => $date]);

                    $old_releases->update(['type' => $cast['media_type']]);
                }
            }
        }
    }

    public function AuthRouteAPI(Request $request)
    {
        return $request->user();
    }

    public function getUserProfile()
    {
        if(Auth::check())
        {
            $profile = User::where('id', Auth::id())->first();
            $image_url = Image::make(storage_path() . '/avatars/' . $profile->avatar)->response();
            $list_number = UserList::where('fk_userlist_user', $profile->id)->count();
            $movie_comment_number = Filmo_komentaras::where('fk_Naudotojas', $profile->id)->count();
            $tvshow_comment_number = TVshowComment::where('fk_com_User', $profile->id)->count();
            $comment_number = $movie_comment_number + $tvshow_comment_number;
            $people_number = PersonMark::where('fk_User', $profile->id)->count();

            $movie_like = $profile->movieLikes;
            //$liked_movies = $profile->likedMovies;
            $liked_movies = array();
            foreach ($movie_like as $like)
            {
                if($like->is_liked === 1)
                    array_push($liked_movies, Filmas::where('id_Filmas', $like->fk_like_Movie)->first());
            }

            $tvshow_likes = $profile->tvshowLikes;
            //$liked_tvshows = $profile->likedTVshows;
            $liked_tvshows = array();
            foreach ($tvshow_likes as $like)
            {
                if($like->is_liked === 1)
                    array_push($liked_tvshows, TvShow::where('id_TVShow', $like->fk_like_TVshow)->first());
            }

            //dd($liked_movies, $liked_tvshows);

            return view('user.profile',
                compact('profile', 'image_url', 'list_number', 'comment_number' , 'people_number', 'liked_movies', 'liked_tvshows'));
        }
        else
        {
            return Redirect::to('/login/');
        }
    }
}
