<?php

namespace App\Http\Controllers;

use App\Filmas;
use App\Genre;
use App\LikeMovie;
use App\MovieGenre;
use App\UserList;
use App\UserListItem;
use App\Watchlist;
use DateTimeZone;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Filmo_komentaras;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Config;
use Tmdb\Api\Movies;
use Tmdb\Laravel\Facades\Tmdb;

class FilmController extends Controller
{
    //TODO make token global
    const API_TOKEN = '6fe4fbead1507fbf062d28ac8cc61f05';

    public function getFilmInfo($id)
    {
        $movie = Filmas::where('id_Filmas', $id)->first();
        $genres = $movie->genres;

        $isInWatchlist = false;
        if(Watchlist::where('fk_Watchlist_Movie', $movie->id_Filmas)->where('fk_Watchlist_User', Auth::id())->exists())
        {
            $isInWatchlist = true;
        }
        $comments = $movie->comments()->where('parent_id', 0)->simplePaginate(10);
        return view('film.film', compact('movie', 'comments', 'genres', 'isInWatchlist'));
    }

    public function getFilmByName($name)
    {
        $film = Filmas::all()->where('id_Filmas', $name)->first();
        return $film;
    }

    public function getFilmAddForm($what_to_add)
    {
        if(Auth::user()->hasRole('administratorius'))
        {
            $allGenre = Genre::all();
            if($what_to_add === '0')
            {
                return view('film.film_form', compact('allGenre'));
            }
            else
            {
                return view('TVshow.TVshow_form', compact('allGenre'));
            }
        }
        else
        {
            //dd(session()->get('username'));
            return redirect()->action('HomeController@index');
        }
    }

    public function saveAPIMovie($movie_to_save, $movie_credits)
    {
        $movie = new Filmas();
        if(Filmas::where('id_Filmas', $movie_to_save['id'])->exists())
        {
            return false;
        }
        else
        {
            $movie->id_Filmas = $movie_to_save['id'];
            $movie->isleidimo_data = $movie_to_save['release_date'];
            $movie->pavadinimas = $movie_to_save['title'];
            $movie->vartotoju_vertinimas = 0;
            $movie->photolink = 'https://image.tmdb.org/t/p/w300_and_h450_bestv2' . $movie_to_save['poster_path'];
            $movie->vertinimas = $movie_to_save['vote_average'];
            $movie->aprasymas = $movie_to_save['overview'];
            $movie->kalba = $movie_to_save['original_language'];

            $director_string = '';
            $writer_string = '';
            foreach ($movie_credits['crew'] as $crew)
            {
                if($crew['job'] == 'Director')
                    $director_string = $director_string . $crew['name'] . ", ";

                if($crew['job'] == 'Screenplay')
                    $writer_string = $writer_string . $crew['name'] . ", ";
            }
            $director_string = substr($director_string,0, -2);
            $writer_string = substr($writer_string, 0,-2);

            $actor_string = '';
            if(count($movie_credits['cast']) > 10)
            {
                for($i = 0; $i <= 10; $i++)
                {
                    $actor_string = $actor_string . $movie_credits['cast'][$i]['name']. ", ";
                }
            }
            else
            {
                foreach ($movie_credits['cast'] as $credit)
                {
                    $actor_string = $actor_string . $credit['name']. ", ";
                }
            }

            $actor_string = substr($actor_string, 0, -2);
            $movie->director_list = $director_string;
            $movie->actor_string = $actor_string;
            $movie->writer_string = $writer_string;
            $movie->save();

            foreach ($movie_to_save['genres'] as $genre)
            {
                $genre_connection = new MovieGenre();
                $genre_connection->fk_gfilmas = $movie->id_Filmas;
                $genre_connection->fk_genre = $genre['id'];
                $genre_connection->save();
            }

            return true;
        }
    }

    public function getMovieAPIData(Request $request)
    {
        if(Auth::user()->hasRole('administratorius'))
        {
            $client = new \Tmdb\Client(new \Tmdb\ApiToken(FilmController::API_TOKEN));
            if((int)$request['movie-id'] !== -1 and $request['movie-id'] !== null )
            {
                try
                {
                    $tmdb_movie_data = $client->getMoviesApi()->getMovie($request['movie-id']);
                    $tmdb_movie_credits = $client->getMoviesApi()->getCredits($request['movie-id']);
                }
                catch(Exception $e)
                {
                    return back()->withErrors('API error message: ' .$e->getMessage());
                }

                $is_added = FilmController::saveAPIMovie($tmdb_movie_data, $tmdb_movie_credits);

                if($is_added)
                {
                    return Redirect::to('/film/addFilm/0')->with('success', 'Movie added');
                }
                else
                {
                    return back()->withErrors('Movie id already exists');
                }
            }
            else if($request['movie-name'] !== null and $request['movie-name'] !== "")
            {
                try
                {
                    $tmdb_movie_search_data = $client->getSearchApi()->searchMovies($request['movie-name']);
                }
                catch(Exception $e)
                {
                    return back()->withErrors('API error message: ' .$e->getMessage());
                }

                if($tmdb_movie_search_data['total_results'] != 0)
                {
                    $earliest_movie = $tmdb_movie_search_data['results'][0];
                    foreach ($tmdb_movie_search_data['results'] as $movie)
                    {
                        if($movie['release_date'] > $earliest_movie['release_date'])
                        {
                            $earliest_movie = $movie;
                        }
                    }

                    try
                    {
                        $tmdb_movie_data = $tmdb_movie_data = $client->getMoviesApi()->getMovie($earliest_movie['id']);
                        $tmdb_movie_credits = $client->getMoviesApi()->getCredits($earliest_movie['id']);
                    }
                    catch(Exception $e)
                    {
                        return back()->withErrors('API error message: ' .$e->getMessage());
                    }

                    $is_added = FilmController::saveAPIMovie($tmdb_movie_data, $tmdb_movie_credits);

                    if($is_added)
                    {
                        return Redirect::to('/film/addFilm/0')->with('success', 'Movie added');
                    }
                    else
                    {
                        return back()->withErrors('Movie id already exists');
                    }
                }
                else
                {
                    return back()->withErrors('No movies with this name');
                }
            }
        }
        else
        {
            return redirect()->action('HomeController@index');
        }
    }

    public function addFilm(Request $request)
    {
        if(Auth::user()->hasRole('administratorius'))
        {
            $validator = Validator::make($request->all(), [
                'id_Filmas' => 'required|unique:movie|numeric',
                'pavadinimas' => 'required|unique:movie|regex:/^[\pL\s\-]+$/u|max:255',
                'rdate' => 'required|date|after:2000-01-01',
                'language' => 'required|alpha',
                'genreselect' => 'array',
                'directors' => 'nullable',
                'actors' => 'nullable',
                'writers' => 'nullable',
                'rating' => 'nullable',
                'poster' => 'required|active_url|max:255',
                'summary' => 'required'
            ]);

            if ($validator->fails())
            {
                $request->flash();
                return Redirect::back()->withErrors($validator);
            }
            else
            {
                $movie = new Filmas();
                $movie->id_Filmas = $request->input('id_Filmas');
                $movie->pavadinimas = $request->input('pavadinimas');
                $movie->isleidimo_data = $request->input('rdate');
                $movie->vertinimas = $request->input('rating');
                $movie->vartotoju_vertinimas = 0;
                $movie->director_list = $request->input('directors');
                $movie->actor_string = $request->input('actors');
                $movie->writer_string = $request->input('writers');
                $movie->aprasymas = $request->input('summary');
                $movie->kalba = $request->input('language');
                $movie->photolink = $request->input('poster');
                $movie->save();

                if($request->input('genres') != null)
                {
                    foreach ($request->input('genres') as $genre)
                    {
                        $genre_connection = new MovieGenre();
                        $genre_connection->fk_gfilmas = $movie->id_Filmas;
                        $genre_connection->fk_genre = $genre;
                        $genre_connection->save();
                    }
                }
                else
                {
                    return Redirect::back()->withErrors("No genres listed");
                }
            }

            return Redirect::to('/film/addFilm/0')->with('success', 'Movie added');
        }
        else
        {
            return redirect()->action('HomeController@index');
        }
    }

    public function getFilmEditForm($id)
    {
        if(Auth::user()->hasRole('administratorius'))
        {
            $allGenre = Genre::all();
            $film = Filmas::where('id_Filmas', $id)->first();
            $film_genres = $film->genres;

            return view('film.edit_film_form', compact('allGenre', 'film', 'film_genres'));
        }
        else
        {
            return redirect()->action('HomeController@index');
        }
    }

    public function editFilm(Request $request, $id)
    {
        if(Auth::user()->hasRole('administratorius'))
        {
            $validator = Validator::make($request->all(), [
                'pavadinimas' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
                'rdate' => 'required|date|after:2000-01-01',
                'language' => 'required|alpha',
                'genreselect' => 'array',
                'directors' => 'nullable',
                'actors' => 'nullable',
                'rating' => 'nullable',
                'writers' => 'nullable',
                'poster' => 'required|active_url|max:255',
                'summary' => 'required'
            ]);

            if ($validator->fails())
            {
                return Redirect::back()->withErrors($validator);
            }
            else
            {
                $data = $request->all();
                $film = Filmas::where('id_Filmas', $id)->first();
                $film->update(['pavadinimas' => $data['pavadinimas']]);
                $film->update(['isleidimo_data' => $data['rdate']]);
                $film->update(['aprasymas' => $data['summary']]);
                $film->update(['kalba' => $data['language']]);
                $film->update(['photolink' => $data['poster']]);
                $film->update(['director_list' => $data['directors']]);
                $film->update(['actor_string' => $data['actors']]);
                $film->update(['writer_string' => $data['writers']]);

                if($data['reset_check'] === 'true')
                {
                    LikeMovie::where('fk_like_Movie', $film->id_Filmas)->delete();
                    $film->update(['vartotoju_vertinimas' => 0]);
                }

                $old_genres = collect($film->genres);
                foreach($data['genres'] as $genre)
                {
                    if(!$old_genres->contains('id_Genre', $genre))
                    {
                        $genre_connection = new MovieGenre();
                        $genre_connection->fk_gfilmas = $film->id_Filmas;
                        $genre_connection->fk_genre = $genre;
                        $genre_connection->save();
                    }
                }

                foreach($old_genres as $genre)
                {
                    if(!collect($data['genres'])->contains($genre['id_Genre']))
                    {
                        MovieGenre::where('fk_gfilmas', $film->id_Filmas)->where('fk_genre', $genre['id_Genre'])->delete();
                    }
                }

            }
            return redirect()->back()->with('success', 'Movie data updated');
        }
        else
        {
            return redirect()->action('HomeController@index');
        }
    }

    public function deleteFilm($id)
    {
        if(Auth::user()->hasRole('administratorius'))
        {
            LikeMovie::where('fk_like_Movie', $id)->delete();
            UserListItem::where('fk_userlistitem_movie', $id)->where('fk_userlistitem_tvshow', null)->delete();
            Watchlist::where('fk_Watchlist_Movie', $id)->where('fk_Watchlist_TVshow', null)->delete();
            MovieGenre::where('fk_gfilmas', $id)->delete();
            Filmo_komentaras::where('fk_Filmas', $id)->delete();
            Filmas::where('id_Filmas', $id)->delete();
            return Redirect::to('/home')->with('success', 'Movie deleted');
        }
        else
        {
            return redirect()->action('HomeController@index');
        }
    }

    public function likeMovie($id, $is_liked)
    {
        if(Auth::check())
        {
            $like = LikeMovie::where('fk_like_User', Auth::id())->where('fk_like_Movie', $id)->first();
            $movie = Filmas::where('id_Filmas', $id)->first();
            if($like === null)
            {
                $movie_like = new LikeMovie();
                $movie_like->is_liked = (int)$is_liked;
                $movie_like->fk_like_User = Auth::id();
                $movie_like->fk_like_Movie = $id;
                $movie_like->save();
                $count = $movie->vartotoju_vertinimas;
                $movie->update(['vartotoju_vertinimas' => $count + (int)$is_liked]);
                return back();
            }
            else
            {
                if($like->is_liked === (int)$is_liked)
                {
                    return back();
                }
                else
                {
                    $like->update(['is_liked' => (int)$is_liked]);
                    $count = $movie->vartotoju_vertinimas;
                    $movie->update(['vartotoju_vertinimas' => $count + (int)$is_liked]);
                    return back();
                }
            }
        }
        else
        {
            return Redirect::to('/login/');
        }
    }
}
