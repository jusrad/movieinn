<?php


namespace App\Http\Controllers;

use App\Genre;
use App\LikeTVshow;
use App\TvShow;
use App\TVshowComment;
use App\TVShowGenre;
use App\UserListItem;
use App\Watchlist;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use App\Filmo_komentaras;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class TvShowController extends Controller
{
    public function getTVshowList()
    {
        $allShows = TvShow::all()->map->only(['id_TVShow', 'title', 'release_date', 'rating', 'posterURL'])->toArray();

        if(count($allShows) === 0)
        {
            $filmchunk = array_chunk($allShows, 4);
            $placeholderfilm = new TvShow();
            array_push($filmchunk, $placeholderfilm);
            return view('TVshow.TVshow_list', ["allFilms" => $filmchunk]);
        }

        for($i = 0; $i < count($allShows); $i++)
        {
            $tmp = Carbon::createFromFormat('Y-m-d', $allShows[$i]['release_date'])->year;
            $allShows[$i]['release_date'] = $tmp;
        }

        $filmchunk = array_chunk($allShows, 4);
        $placeholderfilm = new TvShow();

        for ($x = count($filmchunk[count($filmchunk) - 1]); $x < 4; $x++) {
            array_push($filmchunk[count($filmchunk) - 1], $placeholderfilm);
        }

        return view('TVshow.TVshow_list', ["allFilms" => $filmchunk]);
    }

    public function getTVshowInfo($id)
    {
        $tvshow = TvShow::where('id_TVShow', $id)->first();

        $genres = $tvshow->genres;

        $isInWatchlist = false;
        if(Watchlist::where('fk_Watchlist_TVshow', $tvshow->id_TVShow)->where('fk_Watchlist_User', Auth::id())->exists())
        {
            $isInWatchlist = true;
        }

        $comments = $tvshow->comments()->where('parent_id', 0)->paginate(10);
        return view('TVshow.TVshow', compact('tvshow', 'comments', 'genres', 'isInWatchlist'));
    }

    public function saveAPITvshow($tvshow_to_save, $tvshow_credits)
    {
        $tvshow = new TvShow();
        if(TvShow::where('id_TVShow', $tvshow_to_save['id'])->exists())
        {
            return false;
        }
        else
        {
            $tvshow->id_TVShow = $tvshow_to_save['id'];
            $tvshow->release_date = $tvshow_to_save['first_air_date'];
            $tvshow->title = $tvshow_to_save['name'];
            $tvshow->user_rating = 0;
            $tvshow->posterURL = 'https://image.tmdb.org/t/p/w300_and_h450_bestv2' . $tvshow_to_save['poster_path'];
            $tvshow->rating = $tvshow_to_save['vote_average'];
            $tvshow->overview = $tvshow_to_save['overview'];
            $tvshow->type = $tvshow_to_save['type'];
            $tvshow->season_number = end($tvshow_to_save['seasons'])['season_number'];
            $tvshow->last_air_date = $tvshow_to_save['last_air_date'];

            $director_string = '';
            foreach ($tvshow_to_save['created_by'] as $creators)
            {
                $director_string = $director_string . $creators['name'] . ", ";
            }
            $director_string = substr($director_string,0, -2);

            $writer_string = '';
            foreach ($tvshow_credits['crew'] as $crew)
            {
                if($crew['department'] == 'Writing')
                    $writer_string = $writer_string . $crew['name'] . ", ";
            }
            $writer_string = substr($writer_string, 0,-2);

            $actor_string = '';
            if(count($tvshow_credits['cast']) > 10)
            {
                for($i = 0; $i <= 10; $i++)
                {
                    $actor_string = $actor_string . $tvshow_credits['cast'][$i]['name']. ", ";
                }
            }
            else
            {
                foreach ($tvshow_credits['cast'] as $credit)
                {
                    $actor_string = $actor_string . $credit['name']. ", ";
                }
            }
            $actor_string = substr($actor_string, 0, -2);

            $tvshow->director_list = $director_string;
            $tvshow->actor_string = $actor_string;
            $tvshow->writer_string = $writer_string;
            $tvshow->save();

            foreach ($tvshow_to_save['genres'] as $genre)
            {
                $genre_connection = new TVShowGenre();
                $genre_connection->fk_gtvshow = $tvshow->id_TVShow;
                $genre_connection->fk_genre = $genre['id'];
                $genre_connection->save();
            }

            return true;
        }
    }

    public function getTVShowAPIData(Request $request)
    {
        if(Auth::user()->hasRole('administratorius'))
        {
            $client = new \Tmdb\Client(new \Tmdb\ApiToken(FilmController::API_TOKEN));
            if((int)$request['tvshow-id'] !== -1 and $request['tvshow-id'] !== null )
            {
                try
                {
                    $tmdb_tvshow_data = $client->getTvApi()->getTvshow($request['tvshow-id']);
                    $tmdb_tvshow_credits = $client->getTvApi()->getCredits($request['tvshow-id']);
                }
                catch(Exception $e)
                {
                    return back()->withErrors('API error message: ' .$e->getMessage());
                }

                $is_added = TvShowController::saveAPITvshow($tmdb_tvshow_data, $tmdb_tvshow_credits);

                if($is_added)
                {
                    return Redirect::to('/film/addFilm/1')->with('success', 'TV show added');
                }
                else
                {
                    return back()->withErrors('TV show id already exists');
                }

            }
            else if($request['tvshow-name'] !== null or $request['tvshow-name'] !== "")
            {
                try
                {
                    $tmdb_tvshow_search_data = $client->getSearchApi()->searchTv($request['tvshow-name']);
                }
                catch(Exception $e)
                {
                    return back()->withErrors('API error message: ' .$e->getMessage());
                }

                if($tmdb_tvshow_search_data['total_results'] != 0)
                {
                    $earliest_tvshow = $tmdb_tvshow_search_data['results'][0];
                    foreach ($tmdb_tvshow_search_data['results'] as $tvshow)
                    {
                        if($tvshow['first_air_date'] > $earliest_tvshow['first_air_date'])
                        {
                            $earliest_tvshow = $tvshow;
                        }
                    }

                    try
                    {
                        $tmdb_movie_data = $tmdb_movie_data = $client->getTvApi()->getTvshow($earliest_tvshow['id']);
                        $tmdb_movie_credits = $client->getTvApi()->getCredits($earliest_tvshow['id']);
                    }
                    catch(Exception $e)
                    {
                        return back()->withErrors('API error message: ' .$e->getMessage());
                    }

                    $is_added = TvShowController::saveAPITvshow($tmdb_movie_data, $tmdb_movie_credits);

                    if($is_added)
                    {
                        return Redirect::to('/film/addFilm/1')->with('success', 'TV show added');
                    }
                    else
                    {
                        return back()->withErrors('TV show id already exists');
                    }
                }
                else
                {
                    return back()->withErrors('No TV show with this name');
                }
            }
        }
        else
        {
            return redirect()->action('HomeController@index');
        }
    }

    public function addTVShow(Request $request)
    {
        if(Auth::user()->hasRole('administratorius'))
        {
            $validator = Validator::make($request->all(), [
                'id_tvshow' => 'required|unique:tvshow|numeric',
                'title' => 'required|unique:tvshow|regex:/^[\pL\s\-]+$/u|max:255',
                'rdate' => 'required|date|after:2000-01-01',
                'Lastrdate' => 'required|date',
                'genres' => 'array',
                'directors' => 'nullable',
                'actors' => 'nullable',
                'writers' => 'nullable',
                'rating' => 'nullable',
                'SeasonNr' => 'nullable|numeric',
                'poster' => 'required|active_url|max:255',
                'summary' => 'required'
            ]);

            if ($validator->fails())
            {
                $request->flash();
                //dd($request);
                return Redirect::back()->withErrors($validator);
            }
            else
            {
                $tvshow = new TvShow();
                $tvshow->id_TVShow = $request->input('id_tvshow');
                $tvshow->title = $request->input('title');
                $tvshow->release_date = $request->input('rdate');
                $tvshow->rating = $request->input('rating');
                $tvshow->user_rating = 0;
                $tvshow->director_list = $request->input('directors');
                $tvshow->actor_string = $request->input('actors');
                $tvshow->writer_string = $request->input('writers');
                $tvshow->overview = $request->input('summary');
                $tvshow->posterURL = $request->input('poster');
                $tvshow->type = $request->input('type');
                $tvshow->last_air_date = $request->input('Lastrdate');
                $tvshow->season_number = $request->input('SeasonNr');
                $tvshow->save();

                if($request->input('genres') != null)
                {
                    foreach ($request->input('genres') as $genre)
                    {
                        $genre_connection = new TVShowGenre();
                        $genre_connection->fk_gtvshow = $tvshow->id_TVShow;
                        $genre_connection->fk_genre = $genre;
                        $genre_connection->save();
                    }
                }
                else
                {
                    return Redirect::back()->withErrors("No genres listed");
                }
            }

            return Redirect::to('/film/addFilm/1')->with('success', 'TV show added');
        }
        else
        {
            return redirect()->action('HomeController@index');
        }
    }

    public function getTVshowEditForm($id)
    {
        if(Auth::user()->hasRole('administratorius'))
        {
            $allGenre = Genre::all();
            $tvshow = TvShow::where('id_TVShow', $id)->first();
            $tvshow_genres = $tvshow->genres;

            return view('TVshow.edit_TVshow_form', compact('allGenre', 'tvshow', 'tvshow_genres'));
        }
        else
        {
            return redirect()->action('HomeController@index');
        }
    }

    public function editTVShow(Request $request, $id)
    {
        if(Auth::user()->hasRole('administratorius'))
        {
            $validator = Validator::make($request->all(), [
                'title' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
                'rdate' => 'required|date|after:2000-01-01',
                'Lastrdate' => 'required|date',
                'genres' => 'array',
                'directors' => 'nullable',
                'actors' => 'nullable',
                'writers' => 'nullable',
                'rating' => 'nullable',
                'type' => 'nullable',
                'SeasonNr' => 'nullable|numeric',
                'poster' => 'required|active_url|max:255',
                'summary' => 'required'
            ]);

            if ($validator->fails())
            {
                return Redirect::back()->withErrors($validator);
            }
            else
            {
                $data = $request->all();
                $tvshow = TvShow::where('id_TVShow', $id)->first();

                $tvshow->update(['title' => $data['title']]);
                $tvshow->update(['release_date' => $data['rdate']]);
                $tvshow->update(['overview' => $data['summary']]);
                $tvshow->update(['last_air_date' => $data['Lastrdate']]);
                $tvshow->update(['posterURL' => $data['poster']]);
                $tvshow->update(['director_list' => $data['directors']]);
                $tvshow->update(['actor_string' => $data['actors']]);
                $tvshow->update(['writer_string' => $data['writers']]);
                $tvshow->update(['type' => $data['type']]);
                $tvshow->update(['season_number' => $data['SeasonNr']]);
                $tvshow->update(['rating' => $data['rating']]);

                if($data['reset_check'] === 'true')
                {
                    LikeTVshow::where('fk_like_TVshow', $tvshow->id_TVShow)->delete();
                    $tvshow->update(['user_rating' => 0]);
                }

                if($data['genres'] !== null)
                {
                    $old_genres = collect($tvshow->genres);
                    foreach($data['genres'] as $genre)
                    {
                        if(!$old_genres->contains('id_Genre', $genre))
                        {
                            $genre_connection = new TVShowGenre();
                            $genre_connection->fk_gtvshow = $tvshow->id_TVShow;
                            $genre_connection->fk_genre = $genre;
                            $genre_connection->save();
                        }
                    }

                    foreach($old_genres as $genre)
                    {
                        if(!collect($data['genres'])->contains($genre['id_Genre']))
                        {
                            TVShowGenre::where('fk_gtvshow', $tvshow->id_TVShow)->where('fk_genre', $genre['id_Genre'])->delete();
                        }
                    }
                }
            }
            return redirect()->back()->with('success', 'TV show data updated');
        }
        else
        {
            return redirect()->action('HomeController@index');
        }
    }

    public function deleteTVshow($id)
    {
        if(Auth::user()->hasRole('administratorius'))
        {
            LikeTVshow::where('fk_like_TVshow', $id)->delete();
            UserListItem::where('fk_userlistitem_tvshow', $id)->where('fk_userlistitem_movie', null)->delete();
            Watchlist::where('fk_Watchlist_TVshow', $id)->where('fk_Watchlist_Movie', null)->delete();
            TVShowGenre::where('fk_gtvshow', $id)->delete();
            TVshowComment::where('fk_com_TVshow', $id)->delete();
            TvShow::where('id_TVShow', $id)->delete();
            return Redirect::to('/home')->with('success', 'TV show deleted');
        }
        else
        {
            return redirect()->action('HomeController@index');
        }
    }

    public function likeTVshow($id, $is_liked)
    {
        if(Auth::check())
        {
            $like = LikeTVshow::where('fk_like_User', Auth::id())->where('fk_like_TVshow', $id)->first();
            $tvshow = TvShow::where('id_TVShow', $id)->first();
            if($like === null)
            {
                $movie_like = new LikeTVshow();
                $movie_like->is_liked = (int)$is_liked;
                $movie_like->fk_like_User = Auth::id();
                $movie_like->fk_like_TVshow = $id;
                $movie_like->save();
                $count = $tvshow->user_rating;
                $tvshow->update(['user_rating' => $count + (int)$is_liked]);
                return back();
            }
            else
            {
                if($like->is_liked === (int)$is_liked)
                {
                    return back();
                }
                else
                {
                    $like->update(['is_liked' => (int)$is_liked]);
                    $count = $tvshow->user_rating;
                    $tvshow->update(['user_rating' => $count + (int)$is_liked]);
                    return back();
                }
            }
        }
        else
        {
            return Redirect::to('/login/');
        }
    }
}
