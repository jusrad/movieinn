<?php


namespace App\Http\Controllers;

use App\PeopleNews;
use App\Person;
use App\PersonMark;
use App\User;
use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class PeopleController extends Controller
{
    public function getPeopleList()
    {
        //$people = Person::all()->map->only(['id_Person', 'first_name', 'last_name', 'known_for', 'posterURL'])->toArray();
        $people = Person::all();
        return view('people.people_list', compact('people'));
    }

    public function getPeopleDetails($id)
    {
        $person = Person::where('id_Person', $id)->first();
        $mark = PersonMark::where('id_person', $id)->where('fk_User', Auth::id())->first();

        $isMarked = false;
        if($mark !== null)
            $isMarked = true;

        $client = new \Tmdb\Client(new \Tmdb\ApiToken(FilmController::API_TOKEN));

        try
        {
            $tmdb_person = $client->getPeopleApi()->getPerson($id);
            $tmdb_person_credits = $client->getPeopleApi()->getMovieCredits($id);
        }
        catch(Exception $e)
        {
            $tmdb_person_credits = null;
        }

        $name = explode(" ", $tmdb_person['name']);
        if($name[0] !== $person->first_name and $name[1] !== $person->last_name)
            $tmdb_person_credits = null;

        if($tmdb_person_credits !== null)
        {
            if($person->known_for === 'Acting')
            {
                $credits = $tmdb_person_credits['cast'];
            }
            else
            {
                $credits = $tmdb_person_credits['crew'];
            }

            foreach ($credits as $key => $row) {
                if(array_key_exists('release_date', $row))
                    $dates[$key]  = $row['release_date'];
                else
                    $dates[$key]  = 'Unknown';
            }
            array_multisort($dates, SORT_DESC, $credits);
        }
        else
        {
            $credits = null;
        }

        return view('people.person', compact('person', 'isMarked', 'credits'));
    }

    public function getPersonAddForm()
    {
        if(Auth::user()->hasRole('administratorius'))
        {
            return view('people.person_form');
        }
        else
        {
            return redirect()->action('HomeController@index');
        }
    }

    public function saveAPIPersonData($person_data)
    {
        $person = new Person();
        if(Person::where('id_Person', $person_data['id'])->exists())
        {
            return false;
        }
        else
        {
            $person->id_Person = $person_data['id'];
            $name = explode(" ", $person_data['name']);
            $person->first_name = $name[0];
            $person->last_name = $name[1];
            $person->birthday = $person_data['birthday'];
            $person->image = 'https://image.tmdb.org/t/p/w300_and_h450_bestv2' . $person_data['profile_path'];
            $person->birth_place = $person_data['place_of_birth'];
            $person->biography = $person_data['biography'];
            $person->known_for = $person_data['known_for_department'];
            $person->save();

            return true;
        }
    }

    public function getPersonAPIData(Request $request)
    {
        if(Auth::user()->hasRole('administratorius'))
        {
            $client = new \Tmdb\Client(new \Tmdb\ApiToken(FilmController::API_TOKEN));
            if((int)$request['person-id'] !== -1 and $request['person-id'] !== null )
            {
                try
                {
                    $tmdb_person_data = $client->getPeopleApi()->getPerson($request['person-id']);
                }
                catch(Exception $e)
                {
                    return back()->withErrors('API error message: ' .$e->getMessage());
                }

                $is_saved = PeopleController::saveAPIPersonData($tmdb_person_data);

                if($is_saved)
                {
                    return back()->with('success', 'Person added');
                }
                else
                {
                    return back()->withErrors('Person id already exists');
                }
            }
            else if($request['person-name'] !== null and $request['person-name'] !== "")
            {
                try
                {
                    $tmdb_people_search_data = $client->getSearchApi()->searchPersons($request['person-name']);
                }
                catch(Exception $e)
                {
                    return back()->withErrors('API error message: ' .$e->getMessage());
                }

                if($tmdb_people_search_data['total_results'] != 0)
                {
                    $most_popular_person = $tmdb_people_search_data['results'][0];
                    foreach ($tmdb_people_search_data['results'] as $person)
                    {
                        if($person['popularity'] > $most_popular_person['popularity'])
                        {
                            $most_popular_person = $person;
                        }
                    }

                    try
                    {
                        $tmdb_person_data = $tmdb_movie_data = $client->getPeopleApi()->getPerson($most_popular_person['id']);
                    }
                    catch(Exception $e)
                    {
                        return back()->withErrors('API error message: ' .$e->getMessage());
                    }

                    $is_saved = PeopleController::saveAPIPersonData($tmdb_person_data);

                    if($is_saved)
                    {
                        return back()->with('success', 'Person added');
                    }
                    else
                    {
                        return back()->withErrors('Person id already exists');
                    }
                }
                else
                {
                    return back()->withErrors('No people with this name');
                }
            }
        }
        else
        {
            return redirect()->action('HomeController@index');
        }
    }

    public function addPerson(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_Person' => 'required|unique:people|numeric',
            'first_name' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'last_name' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'bday' => 'required|date',
            'bplace' => 'nullable|alpha',
            'known_for' => 'alpha',
            'person-picture' => 'required|active_url|max:255',
            'biography' => 'required'
        ]);

        if ($validator->fails())
        {
            $request->flash();
            return Redirect::back()->withErrors($validator);
        }
        else
        {
            $person = new Person();

            $person->id_Person = $request->input('id_Person');
            $person->first_name = $request->input('first_name');
            $person->last_name = $request->input('last_name');
            $person->birthday = $request->input('bday');
            $person->known_for = $request->input('known_for');
            $person->biography = $request->input('biography');
            $person->birth_place = $request->input('bplace');
            $person->image = $request->input('person-picture');

            $person->save();
        }

        return Redirect::to('/people/addPerson')->with('success', 'Person added');
    }

    public function getPersonEditForm($id)
    {
        if(Auth::user()->hasRole('administratorius'))
        {
            $person = Person::where('id_Person', $id)->first();
            return view('people.edit_person_form', compact('person'));
        }
        else
        {
            return redirect()->action('HomeController@index');
        }
    }

    public function editPerson(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'last_name' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'bday' => 'required|date',
            'bplace' => 'nullable',
            'known_for' => 'alpha',
            'person-picture' => 'required|active_url|max:255',
            'biography' => 'required'
        ]);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator);
        }
        else
        {
            $data = $request->all();
            $person = Person::where('id_Person', $id)->first();

            $person->update(['first_name' => $data['first_name']]);
            $person->update(['last_name' => $data['last_name']]);
            $person->update(['birthday' => $data['bday']]);
            $person->update(['known_for' => $data['known_for']]);
            $person->update(['biography' => $data['biography']]);
            $person->update(['birth_place' => $data['bplace']]);
            $person->update(['image' => $data['person-picture']]);
        }
        return redirect()->back()->with('success', 'Person data updated');
    }

    public function deletePerson($id)
    {
        if(Auth::user()->hasRole('administratorius'))
        {
            PersonMark::where('id_person', $id)->delete();
            PeopleNews::where('fk_releases_person', $id)->delete();
            Person::where('id_Person', $id)->delete();
            return Redirect::to('/people/peopleList')->with('success', 'Person deleted');
        }
        else
        {
            return redirect()->action('HomeController@index');
        }
    }

    public function getMarkedPeople()
    {
        if(Auth::check())
        {
            $user = User::where('id', Auth::id())->first();
            $marked = $user->marked_people->map->only(['id_person'])->toArray();
            $marked = array_column($marked, 'id_person');
            $people = Person::all()->only($marked);

            return view('user.marked_people', compact('people'));
        }
        else
        {
            return Redirect::to('/login/');
        }
    }

    public function markPerson($id)
    {
        if(Auth::check())
        {
            $old_mark = PersonMark::where('id_person', $id)->where('fk_User', Auth::id())->first();

            if($old_mark === null)
            {
                $mark = new PersonMark();
                $mark->id_person = $id;
                $mark->fk_User = Auth::id();
                $mark->save();
            }

            return Redirect::back()->with('success', 'You will receive emails about new movie and TV show');
        }
        else
        {
            return Redirect::to('/login');
        }
    }

    public function unmarkPerson($id)
    {
        if(Auth::check())
        {
            $old_mark = PersonMark::where('id_person', $id)->where('fk_User', Auth::id())->first();

            if($old_mark !== null)
            {
                $old_mark->delete();
            }

            return Redirect::back()->with('success', 'Person is unmarked');;
        }
        else
        {
            return Redirect::to('/login');
        }
    }
}
