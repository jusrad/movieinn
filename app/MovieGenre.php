<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieGenre extends Model
{
    protected $table = 'genres';

    protected $fillable = [
        'fk_gfilmas',
        'fk_genre'
    ];

    public $timestamps = false;
}
