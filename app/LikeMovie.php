<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeMovie extends Model
{
    protected $table = 'like_movie';

    protected $primaryKey ='id';

    protected $fillable = [
        'is_liked',
        'fk_like_User',
        'fk_like_Movie'
    ];

    public $timestamps = false;
}
