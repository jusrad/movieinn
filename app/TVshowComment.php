<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TVshowComment extends Model
{
    protected $table = 'tvshow_comment';

    protected $primaryKey = 'id_TVshow_comment';

    protected $fillable = [
        'content',
        'create_date',
        'user',
        'parent_id',
        'rating',
        'fk_com_User',
        'fk_com_TVshow'
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User', 'fk_com_User');
    }

    public function movies()
    {
        return $this->belongsTo('App\TVShow', 'fk_com_TVshow');
    }

    public function replies() {
        return $this->hasMany('App\TVshowComment', 'parent_id');
    }
}
