<?php

namespace App\Console\Commands;

use App\Http\Controllers\FilmController;
use App\PeopleNews;
use App\Person;
use Illuminate\Console\Command;

class GetNewReleases extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'releases:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get new movies and upcoming movie/TV show releases of person';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Updating people releases');
        $client = new \Tmdb\Client(new \Tmdb\ApiToken(FilmController::API_TOKEN));
        $people = Person::all()->map->only(['id_Person', 'known_for'])->toArray();

        $bar = $this->output->createProgressBar(count($people));
        $bar->start();

        foreach ($people as $person)
        {
            $tmdb_person_credits = $client->getPeopleApi()->getCombinedCredits($person['id_Person']);
            if($person['known_for'] == 'Acting')
            {
                $this->update($tmdb_person_credits['cast'], $person['id_Person']);
            }
            else
            {
                $this->update($tmdb_person_credits['crew'], $person['id_Person']);
            }

            $bar->advance();
        }

        $bar->finish();
    }

    public function update($array, $id)
    {
        foreach ($array as $cast)
        {
            $date = '';
            $name = '';
            if($cast['media_type'] === 'tv')
            {
                $date = $cast['first_air_date'];
                $name = $cast['name'];
            }
            else if($cast['media_type'] === 'movie')
            {
                $date = $cast['release_date'];
                $name = $cast['title'];
            }

            if($date == null or $date == '' or strtotime($date) > strtotime(date("Y-m-d")))
            {
                //TODO add many-to-many connection table, since many people can be in the same new film
                $old_releases = PeopleNews::where('id_person_releases', $cast['id'])->where('fk_releases_person', $id)->first();
                if($old_releases === null)
                {
                    $new_releases = new PeopleNews();
                    $new_releases->id_person_releases = $cast['id'];
                    $new_releases->name = $name;

                    if($date == '')
                        $new_releases->release_date = null;
                    else
                        $new_releases->release_date = $date;

                    $new_releases->type = $cast['media_type'];
                    $new_releases->fk_releases_person = $id;
                    $new_releases->save();
                }
                else
                {
                    $old_releases->update(['name' => $name]);

                    if($date == '')
                        $old_releases->update(['release_date' => null]);
                    else
                        $old_releases->update(['release_date' => $date]);

                    $old_releases->update(['type' => $cast['media_type']]);
                }
            }
        }
    }
}
