<?php

namespace App\Console\Commands;

use App\Mail\ReleasesShipped;
use App\Person;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send emails about new user marked people releases';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();
        foreach ($users as $user)
        {
            if($user['email_verified_at'] !== null)
            {
                $marked = $user->marked_people;
                $id_array = array();
                foreach ($marked as $mark)
                    array_push($id_array, $mark['id_person']);

                if(count($marked) !== 0)
                {
                    $people = Person::all()->only($id_array);
                    Mail::to($user['email'])->queue(new ReleasesShipped($people, $user['name']));
                }
            }
        }
    }
}
