<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TvShow extends Model
{
    protected $table = 'tvshow';

    protected $primaryKey = 'id_TVShow';

    protected $fillable = [
        'title',
        'release_date',
        'rating',
        'user_rating',
        'overview',
        'type',
        'last_air_date',
        'season_number',
        'director_list',
        'actor_string',
        'writer_string',
        'posterURL'
    ];

    public $timestamps = false;

    public function comments()
    {
        return $this->hasMany('App\TVshowComment', 'fk_com_TVshow');
    }

    public function genres()
    {
        return $this->belongsToMany(
            'App\Genre', 'App\TVShowGenre','fk_gtvshow', 'fk_genre'
        );
    }

    public function likes()
    {
        return $this->hasMany('App\Like_tvshow', 'fk_like_tvshow');
    }
}
