<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReleasesShipped extends Mailable
{
    use Queueable, SerializesModels;

    public $people;
    public $username;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($people, $username)
    {
        $this->people = $people;
        $this->username = $username;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Upcoming movies and TV shows')->view('email.release_email');
    }
}
