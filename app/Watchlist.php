<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Watchlist extends Model
{
    protected $table = 'watchlist';

    protected $fillable = [
        'fk_Watchlist_User',
        'fk_Watchlist_Movie',
        'fk_Watchlist_TVshow'
    ];

    public $timestamps = false;
}
