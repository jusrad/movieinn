<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeTVshow extends Model
{
    protected $table = 'like_tvshow';

    protected $primaryKey ='id_like_tvshow';

    protected $fillable = [
        'is_liked',
        'fk_like_User',
        'fk_like_TVshow'
    ];

    public $timestamps = false;
}
