<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filmas extends Model
{
    protected $table = 'movie';

    protected $primaryKey = 'id_Filmas';

    protected $fillable = [
        'pavadinimas',
        'isleidimo_data',
        'vertinimas',
        'vartotoju_vertinimas',
        'aprasymas',
        'kalba',
        'zanras',
        'director_list',
        'actor_string',
        'writer_string',
        'photolink'
    ];

    public $timestamps = false;

    public function comments()
    {
        return $this->hasMany('App\Filmo_komentaras', 'fk_Filmas');
    }

    public function actors()
    {
        return $this->belongsToMany(
            'App\Aktorius', 'App\vaidina','fk_FilmasV', 'fk_Aktorius'
        );
    }

    public function genres()
    {
        return $this->belongsToMany(
            'App\Genre', 'App\MovieGenre','fk_gfilmas', 'fk_genre'
        );
    }

    public function likes()
    {
        return $this->hasMany('App\LikeMovie', 'fk_like_Movie');
    }

    public function userwholiked()
    {
        return $this->belongsToMany(
            'App\User', 'App\zymi','fk_FilmasZ', 'fk_Naudotojas'
        );
    }
}
