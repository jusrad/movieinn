<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $table = 'genre';

    protected $primaryKey = 'id_Genre';

    protected $fillable = [
        'name'
    ];
}
