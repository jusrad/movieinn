<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Filmo_komentaras extends Model
{
    protected $table = 'movie_comment';

    protected $primaryKey = 'id_FKomentaras';

    protected $fillable = [
        'turinys',
        'sukurimo_data',
        'naudotojas',
        'parent_id',
        'rating',
        'is_deleted',
        'fk_Naudotojas',
        'fk_Filmas'
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User', 'fk_Naudotojas');
    }

    public function movies()
    {
        return $this->belongsTo('App\Filmas', 'fk_Filmas');
    }

    public function replies() {
        return $this->hasMany('App\Filmo_komentaras', 'parent_id');
    }
}
