<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    protected $table = 'users';

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'type',
        'description',
        'mark',
        'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function hasRole($role)
    {
        return $this->type == $role;
    }

    public function MadeFilmComments()
    {
        return $this->hasMany('App\Filmo_komentaras', 'fk_Naudotojas');
    }

    public function movieLikes()
    {
        return $this->hasMany('App\LikeMovie', 'fk_like_User');
    }

    public function tvshowLikes()
    {
        return $this->hasMany('App\LikeTVshow', 'fk_like_User');
    }

    public function likedMovies()
    {
        return $this->belongsToMany('App\Filmas', 'App\LikeMovie', 'fk_like_User', 'fk_like_Movie');
    }

    public function likedTVshows()
    {
        return $this->belongsToMany('App\TvShow', 'App\LikeTVshow', 'fk_like_User', 'fk_like_TVshow');
    }

    public function marked_people()
    {
        return $this->hasMany('App\PersonMark', 'fk_User');
    }
}
