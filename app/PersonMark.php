<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonMark extends Model
{
    protected $table = 'person_mark';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id_person',
        'fk_User'
    ];

    public $timestamps = false;
}
