<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeopleNews extends Model
{
    protected $table = 'people_releases';

    protected $primaryKey = 'id_person_releases';

    protected $fillable = [
        'name',
        'release_date',
        'type',
        'fk_releases_person'
    ];

    public $timestamps = true;
}
