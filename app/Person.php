<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = 'people';

    protected $primaryKey = 'id_Person';

    protected $fillable = [
        'first_name',
        'last_name',
        'birthday',
        'birth_place',
        'image',
        'biography',
        'known_for'
    ];

    public $timestamps = false;

    public function releases()
    {
        return $this->hasMany('App\PeopleNews', 'fk_releases_person');
    }
}
