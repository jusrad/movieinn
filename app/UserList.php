<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserList extends Model
{
    protected $table = 'user_list';

    protected $primaryKey = 'id_user_list';

    protected $fillable = [
        'name',
        'description',
        'fk_userlist_user',
    ];

    public $timestamps = true;
}
