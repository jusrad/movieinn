<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserListItem extends Model
{
    protected $table = 'user_list_item';

    protected $fillable = [
        'fk_userlistitem_list',
        'fk_userlistitem_movie',
        'fk_userlistitem_tvshow'
    ];

    public $timestamps = false;
}
