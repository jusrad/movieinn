<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TVShowGenre extends Model
{
    protected $table = 'tvshow_genre';

    protected $fillable = [
        'fk_gtvshow',
        'fk_genre'
    ];

    public $timestamps = false;
}
