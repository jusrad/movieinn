let FilmBoxes = document.getElementsByClassName("col-md");
for(let i = 0; i < FilmBoxes.length; i++){
    console.log(FilmBoxes[i].id);
    if(FilmBoxes[i].getElementsByTagName("h5")[0].innerText === ""){
        //FilmBoxes[i].style.cssText = "visibility: hidden";
    }
    else{
        FilmBoxes[i].addEventListener('click', ShowFilmInfo)
    }
}

function ShowFilmInfo(filmBox){
    let tmp = this.id;
    let name = this.getElementsByTagName("h5")[0].innerText;
    let x = document.getElementById("info-about-film");
    let current_url = window.location.href
    let request_url = current_url;

    if(current_url.includes('home'))
        request_url = request_url + '/' +  tmp;
    else
        request_url = request_url + 'home/' + tmp;

    //let url = 'http://localhost/MovieInn/public/home/'+tmp;

    fetch(request_url)
        .then(response => {
            return response.json()
        })
        .then(data => {
            console.log(data);
            document.getElementById("film-name").innerText = data["pavadinimas"];
            document.getElementById("img-display").src = data["photolink"];
            document.getElementById("rating").innerText = data["vertinimas"];
            document.getElementById("lang").innerText = data["kalba"];
            document.getElementById("year").innerText = data["isleidimo_data"];
            document.getElementById("director").innerText = data["director_list"];
            document.getElementById("summary").innerText = data["aprasymas"];
            document.getElementsByName('counter')[0].value = data["vartotoju_vertinimas"]
            let tmp = "http://localhost/MovieInn/public/home/film/" + data["id_Filmas"];
            document.getElementById("comment_form").action = tmp;
        })
        .catch(err => {
            console.log(err);
        });

    x.style.display = "block";

    window.onclick = function(event) {
        if (event.target === x) {
            x.style.display = "none";
        }
    }
}
