<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
Route::get('/film/addFilm/auto', 'FilmController@getMovieAPIData');
Route::get('/film/addFilm/{add_what}', 'FilmController@getFilmAddForm') -> name('addFilmForm');
Route::get('/show/addShow/auto', 'TvShowController@getTVShowAPIData');
Route::get('/home/film/delete/{id}', 'FilmController@deleteFilm') -> name('deletefilm');
Route::get('/home/show/delete/{id}', 'TvShowController@deleteTVshow') -> name('deletetvshow');

Route::get('/show/showList', 'TvShowController@getTVshowList');
Route::get('/show/details/{id}', 'TvShowController@getTVshowInfo');

Route::get('/people/addPerson', 'PeopleController@getPersonAddForm') -> name('addPersonForm');
Route::get('/people/addPerson/auto', 'PeopleController@getPersonAPIData');
Route::get('/people/peopleList', 'PeopleController@getPeopleList');
Route::get('/people/details/{id}', 'PeopleController@getPeopleDetails');
Route::get('/people/details/mark/{id}', 'PeopleController@markPerson');
Route::get('/people/details/unmark/{id}', 'PeopleController@unmarkPerson');
Route::get('/people/details/editForm/{id}', 'PeopleController@getPersonEditForm');
Route::post('/people/details/edit/{id}', 'PeopleController@editPerson');
Route::get('/people/details/delete/{id}', 'PeopleController@deletePerson');

Route::get('/home/addUserDataForm/', 'HomeController@getUserDataForm');
Route::post('/home/addUserDataForm/save', 'HomeController@addUser');
Route::get('/home/editForm', 'HomeController@getuserinfo') -> name('edituser');
Route::post('/home/editForm/editUser', 'HomeController@editUser');
Route::get('/home/userlist/delete/{id}', 'HomeController@deleteuser') -> name('deleteuser');
Route::get('/home/userlist', 'HomeController@getuserlist') -> name('userlist');
Route::get('/home/sendEmail', 'HomeController@sendMail') -> name('sendEmail');
Route::get('/home/getReleases', 'HomeController@testReleaseGet') -> name('getReleases');

Route::post('/home/film/postcom', 'CommentController@PostComment');
Route::post('/home/show/postcom', 'CommentController@PostTvshowComment');
Route::post('/FilmForm/addmovie', 'FilmController@addFilm');
Route::post('/FilmForm/addTVShow', 'TvShowController@addTVShow');
Route::post('PersonForm/addPerson', 'PeopleController@addPerson');

Route::get('/home/film/like/{id}/{is_like}', 'FilmController@likeMovie') -> name('like_dislike_movie');
Route::get('/show/details/like/{id}/{is_like}', 'TvShowController@likeTVshow') -> name('like_dislike_tvshow');

//routes for editing tv shows/movies
Route::post('/home/film/edit/change/{id}', 'FilmController@editFilm') -> name('editfilm');
Route::post('/home/show/edit/change/{id}', 'TvShowController@editTVShow') -> name('editShow');
Route::get('/home/film/edit/{id}', 'FilmController@getFilmEditForm') -> name('editfilmform');
Route::get('/home/show/edit/{id}', 'TvShowController@getTVshowEditForm') -> name('editShowForm');

Route::get('/film/delete/comment/delete/{id}', 'CommentController@deleteComment') -> name('deleteComment');
Route::post('/film/delete/comment/edit/{id}', 'CommentController@editComment') -> name('editComment');
Route::get('/show/delete/comment/delete/{id}', 'CommentController@deleteTVShowComment') -> name('deleteShowComment');
Route::post('/show/delete/comment/edit/{id}', 'CommentController@editTVshowComment') -> name('editShowComment');

Route::get('/home/addWatchlist/{id}/{add_what}', 'HomeController@addToWatchlist');
Route::get('/home/removeWatchlist/{id}/{add_what}', 'HomeController@removeFromWatchlist');

Route::get('/home/profile', 'HomeController@getUserProfile');
Route::get('/home/lists', 'HomeController@getLists');
Route::get('/home/lists/list/{id}', 'HomeController@getListItems');
Route::get('/home/lists/list/deleteList/{id}', 'HomeController@deleteUserList');
Route::get('/home/lists/list/{id_list}/remove/{type}/{id}', 'HomeController@removeFromList');
Route::get('/home/lists/listCreateForm', 'HomeController@getCreateForm');
Route::post('home/lists/listCreateForm/create', 'HomeController@createList');
Route::get('/home/markedPeople', 'PeopleController@getMarkedPeople');
Route::post('listAdd/addItem/{add_what}/{id}', 'HomeController@addToLists');
Route::get('/home/addToList/{add_what}/{id}', 'HomeController@getListsToAdd');
Route::get('/home/watchlist', 'HomeController@getWatchlist');
Route::get('/home/about', 'HomeController@getAbout') -> name('aboutPage');
Route::get('/home/film/{id}', 'FilmController@getFilmInfo') -> name('getfilm');
Route::get('/home/{name}', 'FilmController@getFilmByName');
Route::get('/home', 'HomeController@index') -> name('home');
Route::get('/','HomeController@index');

//first
Auth::routes();

//after
Auth::routes(['verify' => true]);
