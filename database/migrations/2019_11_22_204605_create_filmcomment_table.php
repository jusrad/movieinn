<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmcommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filmo_komentaras', function (Blueprint $table) {
            $table->bigIncrements('id_FKomentaras');
            $table->text('turinys')->nullable();
            $table->date('sukurimo_data')->nullable();
            $table->string('naudotojas',255)->nullable();
            $table->bigInteger('fk_Naudotojas', false, true);
            $table->bigInteger('fk_Filmas', false, true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filmo_komentaras');
        Schema::dropIfExists('komentaras');
    }
}
