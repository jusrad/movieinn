<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMovieCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('filmo_komentaras', function(Blueprint $table){
           $table->bigInteger('parent_id', false, true)->nullable();;
           $table->double('rating')->nullable();;
           $table->dropColumn(['created_at', 'updated_at']);
        });

        Schema::rename('filmo_komentaras', 'movie_comment');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('movie_comment', 'filmo_komentaras');

        Schema::table('filmo_komentaras', function(Blueprint $table){
            $table->dropColumn(['rating', 'parent_id']);
            $table->timestamps();
        });
    }
}
