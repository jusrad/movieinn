<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlecommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('naujienu_komentaras', function (Blueprint $table) {
            $table->bigIncrements('id_Nkomentaras');
            $table->text('turinys')->nullable();
            $table->date('sukurimo_data')->nullable();
            $table->bigInteger('fk_Naudotojas', false, true);
            $table->bigInteger('fk_Naujiena', false, true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('naujienu_komentaras');
    }
}
