<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWatchlistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vaidina', function (Blueprint $table) {
            $table->dropForeign('vaidina_fk_filmasv_foreign');
            $table->dropForeign('vaidina_fk_aktorius_foreign');
        });

        Schema::dropIfExists('vaidina');
        Schema::dropIfExists('movie_mark');
        Schema::dropIfExists('aktorius');

        Schema::create('TVShow', function (Blueprint $table) {
            $table->bigIncrements('id_TVShow');
            $table->string('title', 255)->nullable();
            $table->date('release_date')->nullable();
            $table->double('rating')->nullable();
            $table->integer('user_rating')->nullable();
            $table->integer('season_number')->nullable();
            $table->text('overview')->nullable();
            $table->string('type')->nullable();
            $table->date('last_air_date')->nullable();
            $table->string('director_list')->nullable();
            $table->string('posterURL', 255)->nullable();
            $table->string('director_list')->nullable();
            $table->string('writer_string')->nullable();
            $table->string('actor_string')->nullable();
        });

        Schema::create('watchlist', function (Blueprint $table) {
            $table->bigInteger('fk_Watchlist_User', false, true)->nullable();
            $table->bigInteger('fk_Watchlist_Movie', false, true)->nullable();
        });

        Schema::table('watchlist', function (Blueprint $table) {
            $table->foreign('fk_Watchlist_User')->references('id')->on('users');
            $table->foreign('fk_Watchlist_Movie')->references('id_Filmas')->on('movie');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('watchlist', function (Blueprint $table) {
            $table->dropForeign('watchlist_fk_watchlist_user_foreign');
            $table->dropForeign('watchlist_fk_watchlist_movie_foreign');
        });

        Schema::dropIfExists('TVShow');
        Schema::dropIfExists('watchlist');

        Schema::create('movie_mark', function (Blueprint $table) {
            $table->bigInteger('fk_FilmasZ', false, true);
            $table->bigInteger('fk_Naudotojas', false, true);
            $table->timestamps();
        });

        Schema::table('movie_mark', function (Blueprint $table) {
            $table->foreign('fk_FilmasZ')->references('id_Filmas')->on('filmas');
            $table->foreign('fk_Naudotojas')->references('id')->on('users');
        });

        Schema::create('vaidina', function (Blueprint $table) {
            $table->bigInteger('fk_FilmasV', false, true);
            $table->bigInteger('fk_Aktorius', false, true);
        });

        Schema::table('vaidina', function (Blueprint $table) {
            $table->foreign('fk_FilmasV')->references('id_Filmas')->on('filmas');
            $table->foreign('fk_Aktorius')->references('id_Aktorius')->on('aktorius');
        });
    }
}
