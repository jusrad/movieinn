<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNaujienaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('naujiena', function (Blueprint $table) {
            $table->bigIncrements('id_Naujiena');
            $table->string('antraste',255)->nullable();
            $table->text('turinys')->nullable();
            $table->date('sukurimo_data')->nullable();
            $table->text('tekstas')->nullable();
            $table->string('autorius',255)->nullable();
            $table->string('saltinis',255)->nullable();
            $table->string('saltinio_vardas',255)->nullable();
            $table->string('linkphoto',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('naujiena');
    }
}
