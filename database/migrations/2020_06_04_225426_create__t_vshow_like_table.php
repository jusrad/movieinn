<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTVshowLikeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_tvshow', function (Blueprint $table) {
            $table->bigIncrements('id_like_tvshow');
            $table->boolean('is_liked');
            $table->bigInteger('fk_like_User', false, true);
            $table->bigInteger('fk_like_TVshow', false, true);
        });

        Schema::table('like_tvshow', function (Blueprint $table){
            $table->foreign('fk_like_User')->references('id')->on('users');
            $table->foreign('fk_like_TVshow')->references('id_TVShow')->on('tvshow');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('like_tvshow', function (Blueprint $table){
            $table->dropForeign('like_movie_fk_like_user_foreign');
            $table->dropForeign('like_movie_fk_like_tvshow_foreign');
        });

        Schema::dropIfExists('like_tvshow');
    }
}
