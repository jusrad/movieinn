<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCommentsAttribute extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movie_comment', function (Blueprint $table){
           $table->boolean('is_deleted')->default(false);
        });

        Schema::table('tvshow_comment', function (Blueprint $table){
            $table->boolean('is_deleted')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movie_comment', function (Blueprint $table){
            $table->dropColumn('is_deleted');
        });

        Schema::table('tvshow_comment', function (Blueprint $table){
            $table->dropColumn('is_deleted');
        });
    }
}
