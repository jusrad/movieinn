<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filmas', function (Blueprint $table) {
            $table->bigIncrements('id_Filmas');
            $table->string('pavadinimas', 255)->nullable();
            $table->date('isleidimo_data')->nullable();
            $table->double('vertinimas')->nullable();
            $table->integer('vartotoju_vertinimas')->nullable();
            $table->text('aprasymas')->nullable();
            $table->string('kalba', 255)->nullable();
            $table->string('zanras', 255)->nullable();
            $table->string('rezisierius',255)->nullable();
            $table->string('photolink', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filmas');
    }
}
