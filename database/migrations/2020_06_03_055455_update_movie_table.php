<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMovieTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movie', function(Blueprint $table){
            $table->string('director_list')->nullable();
            $table->string('writer_string')->nullable();
            $table->string('actor_string')->nullable();
            $table->dropColumn('rezisierius');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movie', function(Blueprint $table){
            $table->dropColumn('director_list');
            $table->dropColumn('writer_string');
            $table->dropColumn('actor_string');
            $table->string('rezisierius')->nullable();
        });
    }
}
