<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TvshowGenreConnection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tvshow_genre', function (Blueprint $table) {
            $table->foreign('fk_gtvshow')->references('id_TVShow')->on('tvshow');
            $table->foreign('fk_genre')->references('id_Genre')->on('genre');
        });

        Schema::table('watchlist', function (Blueprint $table) {
            $table->bigInteger('fk_Watchlist_TVshow', false, true)->nullable();
        });

        Schema::table('watchlist', function (Blueprint $table) {
            $table->foreign('fk_Watchlist_TVshow')->references('id_TVShow')->on('tvshow');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tvshow_genre', function (Blueprint $table) {
            $table->dropForeign('tvshow_genre_fk_gtvshow_foreign');
            $table->dropForeign('tvshow_genre_fk_genre_foreign');
        });

        Schema::table('watchlist', function (Blueprint $table) {
            $table->dropForeign('watchlist_fk_watchlist_tvshow_foreign');
        });

        Schema::table('watchlist', function (Blueprint $table) {
            $table->dropColumn('fk_Watchlist_TVshow');
        });
    }
}
