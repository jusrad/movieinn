<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('movie_mark');

        Schema::create('people_releases', function (Blueprint $table){
            $table->bigIncrements('id_person_releases');
            $table->string('name');
            $table->date('release_date')->nullable();
            $table->string('type');
            $table->bigInteger('fk_releases_person', false, true);
            $table->timestamps();
        });

        Schema::table('people_releases', function (Blueprint $table){
            $table->foreign('fk_releases_person')->references('id_Person')->on('people');
        });

        Schema::create('user_list', function (Blueprint $table){
            $table->bigIncrements('id_user_list');
            $table->string('name');
            $table->text('description');
            $table->bigInteger('fk_userlist_user', false, true);
            $table->timestamps();
        });

        Schema::table('user_list', function (Blueprint $table){
            $table->foreign('fk_userlist_user')->references('id')->on('users');
        });

        Schema::create('user_list_item', function (Blueprint $table){
            $table->bigInteger('fk_userlistitem_list', false, true);
            $table->bigInteger('fk_userlistitem_movie', false, true)->nullable();
            $table->bigInteger('fk_userlistitem_tvshow', false, true)->nullable();
        });

        Schema::table('user_list_item', function (Blueprint $table){
            $table->foreign('fk_userlistitem_list')->references('id_user_list')->on('user_list');
            $table->foreign('fk_userlistitem_movie')->references('id_Filmas')->on('movie');
            $table->foreign('fk_userlistitem_tvshow')->references('id_TVShow')->on('tvshow');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('movie_mark', function (Blueprint $table) {
            $table->bigInteger('fk_FilmasZ', false, true);
            $table->bigInteger('fk_Naudotojas', false, true);
            $table->timestamps();
        });

        Schema::table('movie_mark', function (Blueprint $table) {
            $table->foreign('fk_FilmasZ')->references('id_Filmas')->on('movie');
            $table->foreign('fk_Naudotojas')->references('id')->on('users');
        });

        Schema::table('user_list_item', function (Blueprint $table){
            $table->dropForeign('user_list_item_fk_userlistitem_list_foreign');
            $table->dropForeign('user_list_item_fk_userlistitem_movie_foreign');
            $table->dropForeign('user_list_item_fk_userlistitem_tvshow_foreign');
        });

        Schema::table('user_list', function (Blueprint $table){
            $table->dropForeign('user_list_fk_userlist_user_foreign');
        });

        Schema::table('people_releases', function (Blueprint $table){
            $table->dropForeign('people_releases_fk_releases_person_foreign');
        });

        Schema::dropIfExists('user_list_item');
        Schema::dropIfExists('user_list');
        Schema::dropIfExists('people_releases');
    }
}
