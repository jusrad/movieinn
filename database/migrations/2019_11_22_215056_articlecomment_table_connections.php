<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ArticlecommentTableConnections extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('naujienu_komentaras', function (Blueprint $table) {
            $table->foreign('fk_Naudotojas')->references('id')->on('users');
            $table->foreign('fk_Naujiena')->references('id_Naujiena')->on('naujiena');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
