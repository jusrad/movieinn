<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTVshowCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tvshow_comment', function (Blueprint $table) {
            $table->bigIncrements('id_TVshow_comment');
            $table->text('content')->nullable();
            $table->date('create_date')->nullable();
            $table->string('user',255)->nullable();
            $table->bigInteger('parent_id', false, true)->nullable();;
            $table->double('rating')->nullable();;
            $table->bigInteger('fk_com_User', false, true);
            $table->bigInteger('fk_com_TVshow', false, true);
        });

        Schema::create('tvshow_genre', function (Blueprint $table) {
            $table->bigInteger('fk_gtvshow', false, true);
            $table->integer('fk_genre', false, true);
        });

        Schema::table('tvshow_comment', function (Blueprint $table) {
            $table->foreign('fk_com_User')->references('id')->on('users');
            $table->foreign('fk_com_TVshow')->references('id_TVshow')->on('tvshow');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tvshow_comment', function (Blueprint $table) {
            $table->dropForeign('tvshow_comment_fk_com_user_foreign');
            $table->dropForeign('tvshow_comment_fk_com_tvshow_foreign');
        });

        Schema::dropIfExists('tvshow_comment');
        Schema::dropIfExists('tvshow_genre');
    }
}
