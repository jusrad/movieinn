<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VaidinaTableConnections extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vaidina', function (Blueprint $table) {
            $table->foreign('fk_FilmasV')->references('id_Filmas')->on('filmas');
            $table->foreign('fk_Aktorius')->references('id_Aktorius')->on('aktorius');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
