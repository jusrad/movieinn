<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FilmcommentTableConnections extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('filmo_komentaras', function (Blueprint $table) {
            $table->foreign('fk_Naudotojas')->references('id')->on('users');
            $table->foreign('fk_Filmas')->references('id_Filmas')->on('filmas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
