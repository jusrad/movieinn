<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('movie_genre', 'genre');
        Schema::rename('filmas', 'movie');
        Schema::rename('zymi', 'movie_mark');
        Schema::dropIfExists('naujienu_komentaras');
        Schema::dropIfExists('naujiena');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('genre', 'movie_genre');
        Schema::rename('movie', 'filmas');
        Schema::rename('movie_mark', 'zymi');

        Schema::create('naujiena', function (Blueprint $table) {
            $table->bigIncrements('id_Naujiena');
            $table->string('antraste',255)->nullable();
            $table->text('turinys')->nullable();
            $table->date('sukurimo_data')->nullable();
            $table->text('tekstas')->nullable();
            $table->string('autorius',255)->nullable();
            $table->string('saltinis',255)->nullable();
            $table->string('saltinio_vardas',255)->nullable();
            $table->string('linkphoto',255)->nullable();
            $table->timestamps();
        });

        Schema::create('naujienu_komentaras', function (Blueprint $table) {
            $table->bigIncrements('id_Nkomentaras');
            $table->text('turinys')->nullable();
            $table->date('sukurimo_data')->nullable();
            $table->bigInteger('fk_Naudotojas', false, true);
            $table->bigInteger('fk_Naujiena', false, true);
            $table->timestamps();
        });
    }
}
