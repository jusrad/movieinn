<?php

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('naujiena')->delete();

        $sql = 'database\Sql\newstableinsert.sql';
        DB::unprepared(file_get_contents($sql));
        $this->command->info('News table seeded!');
    }
}
