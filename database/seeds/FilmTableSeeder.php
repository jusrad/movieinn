<?php

use Illuminate\Database\Seeder;

class FilmTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('filmas')->delete();

        $sql = 'database\Sql\filmtableinsert.sql';
        DB::unprepared(file_get_contents($sql));
        $this->command->info('Film table seeded!');
    }
}
