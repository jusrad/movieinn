<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
                'name' => 'Admin',
                'email' => 'admin@mail.lt',
                'type' => 'administratorius',
                'password' => bcrypt('1234567890')
            ], [
                'name' => 'User',
                'email' => 'user@mail.lt',
                'type' => 'naudotojas',
                'password' => bcrypt('123456789')
        ]);
    }
}
