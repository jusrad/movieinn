@section('search')
    <script src="{{ asset('js/search_movie.js') }}" defer></script>
@endsection

@section('css-file')
    <link rel=stylesheet href={{ asset('css/search.css') }}>
@endsection

<div class="search-container">
    <form id="film-search-form" autocomplete="off" method="get" action="{{url('/film/addFilm/auto')}}">
        <div style="display: inline-block" class="input-field">
            <input id="search-input" type="text" placeholder="Enter movie name" name="movie-name">
            <input id="search-selection-id" type="hidden" value="-1" name="movie-id">
        </div>
        <button class="ml-2" type="submit">Add movie</button>
    </form>
</div>

