@extends('layouts.app')

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
@endsection

@section('css-file')
    <link href="{{ asset('css/movie_list.css') }}" rel="stylesheet">
@endsection

@section('content')
    <section>
        <div style="margin: 0 12px">
            <h1 class="centre-text text-white">Popular movies</h1>
            <div class="container-fluid">
                @foreach($allFilms as $chunk)
                    <div class="row my-2">
                        @foreach($chunk as $film)
                            <div id="{{$film['id_Filmas']}}" @if($film['id_Filmas'] === null) style="visibility: hidden;" @endif  class="col-md col-sm bg-secondary mx-1">
                                <div class="row">
                                    <div class="col-md-6 col-sm p-0">
                                        <img alt="{{$film['pavadinimas']}} poster" class="poster-size" src="{{$film['photolink']}}">
                                    </div>
                                    <div class="col-md-6 col-sm py-2 centre-text">
                                        <div style="height: 20%" class="d-flex flex-row align-items-start justify-content-center">
                                            <div>
                                                <h5 id="name_{{$film['id_Filmas']}}" class="font-weight-bold text-white">{{$film['pavadinimas']}}</h5>
                                            </div>
                                        </div>
                                        <div style="height: 80%" class="d-flex align-items-end justify-content-center">
                                            <div class="mt-auto">
                                                <div class="IMDB-rating">TMDb {{$film['vertinimas']}}</div>
                                                <div class="text-white">{{$film['isleidimo_data']}}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <div id="info-about-film" class="dropdown-content">
        <div class="bg-light border appearing-content-container-size appearing-content-container-margin">
            <div class="container">
                <div class="row my-2">
                    <div class="col-3">
                        <img class="poster_image_size" src="https://www.theprintworks.com/wp-content/themes/psBella/assets/img/film-poster-placeholder.png" id="img-display" alt="film-poster">
                    </div>
                    <div class="col-9">
                        <div class="d-flex justify-content-center">
                            <h5 class="font-weight-bold" id="film-name">Name</h5>
                        </div>
                        <div>
                            Release date: <b id="year"></b><br>
                            Language: <b id="lang"></b><br>
                            IMDb rating: <b id="rating"></b><br>
                            Directors: <b id="director"></b><br>
                        </div>
                        <div class="mt-2">
                            <input class="counter" name="counter" type="hidden" value="0" />
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col">
                        Summary:<p id="summary"></p>
                        <br>
                    </div>
                </div>
                <div class="row m-2">
                    <form id="comment_form" style="width: 100%" method="get">
                        <button class="btn btn-secondary btn-lg btn-block">Comments</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
