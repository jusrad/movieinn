@extends('layouts.app')

@section('content')
    <section>
        <div class="text-white container-fluid">
            <h1 style="text-align: center">Add a new movie</h1>

            <h2>Add movie by name from TMDb</h2>
            @include('film.film_search')
            <hr style="border-color: white">
            <h2>Add movie manually</h2>
            <form method="post" action="{{url('FilmForm/addmovie')}}">
                @csrf
                <div style="width: 20%" class="form-group">
                    <label for="idFilmas">TMDb id</label>
                    <input type="number" class="form-control" id="idFilmas" name="id_Filmas" required value="{{ old('id_Filmas')}}">
                </div>
                <div style="width: 50%" class="form-group">
                    <label for="filmName">Name</label>
                    <input type="text" class="form-control" id="filmName" name="pavadinimas" required value="{{ old('pavadinimas')}}">
                </div>
                <div class="form-row">
                    <div class="col">
                        <div class="form-group">
                            <label for="rdate">Release date</label><br>
                            <input style="width: 50%" type="date" id="rdate" name="rdate" required value="{{ old('rdate')}}">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="lang">Language</label>
                            <select class="form-control" id="lang" name="language">
                                <option value="English">English</option>
                                <option value="French">French</option>
                                <option value="Korean">Korean</option>
                                <option value="Japanese">Japanese</option>
                                <option value="Chinese">Chinese</option>
                                <option value="Russian">Russian</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="director_list_string">Directors (separate by comma)</label>
                    <input type="text" class="form-control" id="director_list_string" name="directors" value="{{ old('directors')}}">
                </div>
                <div class="form-group">
                    <label for="actor_list_string">Actors (separate by comma)</label>
                    <input type="text" class="form-control" id="actor_list_string" name="actors" value="{{ old('actors')}}">
                </div>
                <div class="form-group">
                    <label for="writer_list_string">Writers (separate by comma)</label>
                    <input type="text" class="form-control" id="writer_list_string" name="writers" value="{{ old('writers')}}">
                </div>
                <div class="form-row">
                    <div class="col">
                        <div style="width: 50%" class="form-group">
                            <label for="movie_genres">Select genres</label>
                            <select style="height: 180px"  multiple class="form-control" id="movie_genres" name="genres[]">
                                @foreach($allGenre as $key => $genre)
                                    <option value="{{$genre['id_Genre']}}" {{collect(old('genres'))->contains($genre['id_Genre']) ? 'selected' : ''}}>{{$genre['name']}}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div style="width: 50%" class="form-group">
                            <label for="movie_rating">Movie rating</label>
                            <input type="number" step="0.1" class="form-control" id="movie_rating" name="rating" value="{{ old('rating')}}">
                        </div>
                    </div>
                </div>
                <div style="width: 50%" class="form-group">
                    <label for="posterURL">Movie poster URL</label>
                    <input type="url" class="form-control" id="posterURL" name="poster" value="{{old('poster')}}">
                </div>
                <div class="form-group">
                    <label for="movie_summ">Summary</label>
                    <textarea class="form-control" id="movie_summ" name="summary" rows="15">{{old('summary')}}</textarea>
                </div>

                <button class="btn btn-light" type="submit">Add the movie</button>
            </form>
        </div>
    </section>
@endsection

