@extends('layouts.app')

@section('content')
    <section>
        <div class="text-white container-fluid">
            <h1 style="text-align: center">Edit movie data</h1>
            <form method="post" action="{{route('editfilm', $film->id_Filmas)}}">
                @csrf
                <div class="form-group">
                    <label for="filmName">Name</label>
                    <input type="text" class="form-control" id="filmName" name="pavadinimas" value="{{$film->pavadinimas}}" required>
                </div>
                <div class="form-row">
                    <div class="col">
                        <div class="form-group">
                            <label for="rdate">Release date</label><br>
                            <input style="width: 50%" type="date" id="rdate" name="rdate" value="{{$film->isleidimo_data}}" required>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="lang">Language</label>
                            <select class="form-control" id="lang" name="language">
                                <option value="English" {{$film->kalba == 'English' ? 'selected' : '' }} >English</option>
                                <option value="French" {{$film->kalba == 'French' ? 'selected' : '' }} >French</option>
                                <option value="Korean" {{$film->kalba == 'Korean' ? 'selected' : '' }} >Korean</option>
                                <option value="Japanese" {{$film->kalba == 'Japanese' ? 'selected' : '' }} >Japanese</option>
                                <option value="Chinese" {{$film->kalba == 'Chinese' ? 'selected' : '' }} >Chinese</option>
                                <option value="Russian" {{$film->kalba == 'Russian' ? 'selected' : '' }} >Russian</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="director_list_string">Directors (separate by comma)</label>
                    <input type="text" class="form-control" id="director_list_string" name="directors" value="{{ $film->director_list}}">
                </div>
                <div class="form-group">
                    <label for="actor_list_string">Actors (separate by comma)</label>
                    <input type="text" class="form-control" id="actor_list_string" name="actors" value="{{ $film->actor_string}}">
                </div>
                <div class="form-group">
                    <label for="writer_list_string">Writers (separate by comma)</label>
                    <input type="text" class="form-control" id="writer_list_string" name="writers" value="{{ $film->writer_string}}">
                </div>
                <div class="form-row">
                    <div class="col">
                        <div style="width: 50%" class="form-group">
                            <label for="movie_genres">Select genres</label>
                            <select style="height: 180px" multiple class="form-control" id="movie_genres" name="genres[]">
                                @foreach($allGenre as $key => $genre)
                                    <option value="{{$genre['id_Genre']}}" {{$film_genres->contains('id_Genre', $genre->id_Genre) ? 'selected' : ''}}>{{$genre['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div style="width: 50%" class="form-group">
                            <label for="movie_rating">Movie rating</label>
                            <input type="number" step="0.1" class="form-control" id="movie_rating" name="rating" value="{{ $film->vertinimas}}">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="movie_poster">Movie poster URL</label>
                    <input type="url" class="form-control" id="movie_poster" name="poster" value="{{$film->photolink}}">
                </div>
                <div class="form-group">
                    <label for="summary">Summary</label>
                    <textarea class="form-control" id="summary" name="summary" rows="3">{{$film->aprasymas}}</textarea>
                </div>
                <div class="form-group">
                    <label>Do you want to reset user vote count?</label>
                    <br>
                    <input type="radio" id="reset_yes" name="reset_check" value="true">
                    <label for="reset_yes">Yes</label>
                    <br>
                    <input type="radio" id="reset_no" name="reset_check" value="false" checked>
                    <label for="reset_no">No</label>
                </div>
                <button class="btn btn-light" type="submit">Edit movie data</button>
            </form>
        </div>
    </section>
@endsection
