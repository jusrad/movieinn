@extends('layouts.app')

@section('style')
    .img-size{
        height: 300px;
        width: 600px;
    }
@endsection

@section('content')
    <section class="container-fluid text-white">
        <div>
            <div>
                <h2 class="centre-text">About MoviesInn</h2>
            </div>
            <hr>
            <h3>Data</h3>
            <div class="row">
                <div class="col-10">
                    <p>
                        All film-related metadata used in MovieInn, including actor, director and synopses,
                        release dates and poster art is supplied by The Movie Database (TMDb).
                        <br />
                        <br />
                        <img src="https://a.ltrbxd.com/img/editorial/tmdb-2020.svg" alt="TMDb logo" width="275" height="200">
                        <br />
                        <br />
                        MovieInn uses the TMDb API but is not endorsed or certified by TMDb.
                    </p>
                </div>
            </div>
            <h3>Contacts</h3>
            <div class="row">
                <div class="mt-2 ml-3">
                    Email: <b>jradkevicius@gmail.com</b><br>
                </div>
            </div>
        </div>
    </section>
@endsection
