@extends('layouts.app')

@section('content')
    <section>
        <div class="text-white container-fluid">
            <h1 style="text-align: center">Add a new movie</h1>

            <form method="post" action="{{url('home/lists/listCreateForm/create')}}">
                @csrf
                <div style="width: 50%" class="form-group">
                    <label for="list_name">List Name</label>
                    <input type="text" class="form-control" id="list_name" name="listName" required value="{{ old('listName')}}">
                </div>
                <div class="form-group">
                    <label for="Description">Description</label>
                    <textarea class="form-control" id="Description" name="description" rows="10">{{old('description')}}</textarea>
                </div>

                <button class="btn btn-light" type="submit">Create list</button>
            </form>
        </div>
    </section>
@endsection
