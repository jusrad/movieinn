@extends('layouts.app')

@section('css-file')
    <link href="{{ asset('css/list.css') }}" rel="stylesheet">
@endsection

@section('content')
    <section class="container-fluid text-white">
        <h1 class="centre-text">{{$list['name']}}</h1>
        <hr style="background-color: white">
        <div class="row m-2">{{$list['description']}}</div>
        <table class="table table-dark">
            <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col">Title</th>
                <th scope="col">Type</th>
                <th scope="col">Release date</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                @if($item->getTable() === 'movie')
                    <tr>
                        <td><img style="height: 30%" id="img-display" alt="{{$item->pavadinimas}}_picture" src="{{$item->photolink}}"></td>
                        <td style="vertical-align: middle">{{$item->pavadinimas}}</td>
                        <td style="vertical-align: middle">Movie</td>
                        <td style="vertical-align: middle">{{$item->isleidimo_data}}</td>
                        <td style="vertical-align: middle">
                            <a href="{{route('getfilm', $item->id_Filmas)}}">Details</a>
                            |
                            <a href="{{url('/home/lists/list/'.$list['id_user_list'].'/remove/1/'.$item->id_Filmas)}}">Remove</a>
                        </td>
                    </tr>
                @else
                    <tr>
                        <td><img style="height: 30%" id="img-display" alt="{{$item->title}}_picture" src="{{$item->posterURL}}"></td>
                        <td style="vertical-align: middle">{{$item->title}}</td>
                        <td style="vertical-align: middle">TV show</td>
                        <td style="vertical-align: middle">{{$item->release_date}}</td>
                        <td style="vertical-align: middle">
                            <a href="{{url("/show/details/". $item->id_TVShow)}}">Details</a>
                            |
                            <a href="{{url('/home/lists/list/'.$list['id_user_list'].'/remove/2/'.$item->id_TVShow)}}">Remove</a>
                        </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </section>
@endsection
