@extends('layouts.app')

@section('content')
    <section>
        <div class="container-fluid text-white">
            <h1 class="centre-text">User list</h1>
            <table class="table text-white">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Username</th>
                    <th scope="col">Email</th>
                    <th scope="col">Created</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <th scope="row"> {{ $loop->iteration }}</th>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->created_at}}</td>
                        @if(Auth::id() === $user->id)
                            <td></td>
                        @else
                            <td><a href="{{ route('deleteuser', $user->id) }}">Delete</a></td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
@endsection
