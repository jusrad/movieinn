@extends('layouts.app')

@section('content')
    <section>
        <div class="text-white container-fluid">
            <h1 style="text-align: center">Select your list</h1>
            <form method="post" action="{{url('listAdd/addItem/'.$add_what.'/'.$id)}}">
                @csrf
                <div class="form-row">
                    <div style="width: 50%; margin: auto" class="form-group">
                        <select style="height: 180px"  multiple class="form-control" id="user_lists" name="lists[]">
                            @foreach($lists as $list)
                                <option value="{{$list['id_user_list']}}" {{$in_list->contains('id_user_list', $list['id_user_list']) ? 'selected' : ''}}>{{$list['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="mt-2 row">
                    <div class="col-3"></div>
                    <div class="col">
                        <button class="btn btn-danger" type="button" onclick="window.location='{{url('/home/')}}'">Cancel</button>
                        <button class=" float-right btn btn-primary" type="submit">Add</button>
                    </div>
                    <div class="col-3"></div>
                </div>
            </form>
        </div>
    </section>
@endsection
