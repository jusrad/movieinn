@extends('layouts.app')

@section('css-file')
    <link href="{{ asset('css/lists.css') }}" rel="stylesheet">
@endsection

@section('content')
    <section class="container-fluid text-white">
        <h1 class="centre-text">Your lists</h1>
        <div class="row ml-2 mb-4">
            <a class="btn btn-info" href="{{url('/home/lists/listCreateForm')}}">Create a new list</a>
        </div>
        <div class="flex-container">
            @foreach($lists as $list)
                <div style="background-color: #6b778d" class="row border border-light rounded mb-2 mx-2 " onclick="window.location='{{url('/home/lists/list/'. $list['id_user_list'])}}'">
                    <div class="col-1">
                        <img style="height: 100px; width: 100px" alt="default_list_image" src="https://icons-for-free.com/iconfiles/png/512/clapper+cut+director+making+movie+take+icon-1320195777589696004.png">
                    </div>
                    <div class="col-2 column-content">
                        <span>{{$list['name']}}</span>
                    </div>
                    <div class="col-8"></div>
                    <div class="col-1">
                        <div style="margin-top:35px">
                            <a style="font-size: 15px;" href="{{url('/home/lists/list/deleteList/'.$list['id_user_list'])}}">Delete</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
@endsection
