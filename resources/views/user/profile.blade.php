@extends('layouts.app')

@section('content')
    <section class="container-fluid text-white">
        <h1 class="centre-text">{{$profile->name}} profile</h1>
        <hr style="border-color: white">
        <div class="row my-2">
            <div class="col-2">
                <img id="img-display" alt="{{$profile->name}}-image" src="{{ asset('storage/'.$profile->avatar)}}">
            </div>
            <div class="col-10">
                Email: {{$profile->email}}<br>
                Tag: {{$profile->mark}}<br>
                Description: {{$profile->description}}<br>
            </div>
        </div>
        <h2 class="centre-text">Statistics</h2>
        <div class="row my-2">
            <div class="col-12">
                Number of lists: {{$list_number}}<br>
                Number of comments made: {{$comment_number}}<br>
                Number of people marked: {{$people_number}}<br>
            </div>
        </div>
        <h2 class="centre-text">Liked movies and TV shows</h2>
        <h3 class="centre-text">Liked movies</h3>
        <table class="table table-dark">
            <thead>
            <tr>
                <th scope="col">Title</th>
                <th scope="col">Release date</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($liked_movies as $movie)
                <tr>
                    <td>{{$movie->pavadinimas}}</td>
                    <td>{{$movie->isleidimo_data}}</td>
                    <td>
                        <a href="{{route('getfilm', $movie->id_Filmas)}}">Details</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <h3 class="centre-text">Liked TV shows</h3>
        <table class="table table-dark">
            <thead>
            <tr>
                <th scope="col">Title</th>
                <th scope="col">Release date</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($liked_tvshows as $movie)
                <tr>
                    <td>{{$movie->title}}</td>
                    <td>{{$movie->release_date}}</td>
                    <td>
                        <a href="{{url("/show/details/". $movie->id_TVShow)}}">Details</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </section>
@endsection
