@extends('layouts.app')

@section('content')
    <section class="container-fluid text-white">
        <h1 class="centre-text">Movies and TV shows added to watchlist</h1>
        <table class="table table-dark">
            <thead>
            <tr>
                <th scope="col">Title</th>
                <th scope="col">Release date</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($movies as $movie)
                @if($movie->getTable() === 'movie')
                    <tr>
                        <td>{{$movie->pavadinimas}}</td>
                        <td>{{$movie->isleidimo_data}}</td>
                        <td>
                            <a href="{{route('getfilm', $movie->id_Filmas)}}">Details</a>
                            <a href="{{url('/home/removeWatchlist/'.$movie->id_Filmas.'/'.'0')}}">Remove</a>
                        </td>
                    </tr>
                @else
                    <tr>
                        <td>{{$movie->title}}</td>
                        <td>{{$movie->release_date}}</td>
                        <td>
                            <a href="{{url("/show/details/". $movie->id_TVShow)}}">Details</a>
                            <a href="{{url('/home/removeWatchlist/'.$movie->id_TVShow.'/'.'1')}}">Remove</a>
                        </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </section>
@endsection
