@extends('layouts.app')

@section('style')
    .img-size{
    height: 300px;
    width: 600px;
    }

    .spacing{
    height: 46px;
    }
@endsection

@section('content')
    <section>
        <div class="text-white container-fluid">
            <h1 style="text-align: center">Add profile data</h1>
            <form method="post" enctype="multipart/form-data" action="{{url('/home/addUserDataForm/save')}}">
                @csrf
                <div class="form-group">
                    <label for="user_tag">Tag</label>
                    <input style="width: 50%" type="text" class="form-control" id="user_tag" name="userTag" value="{{ old('userTag')}}">
                </div>
                <div style="width: 20%" class="form-group">
                    <label for="profile_pic">User profile picture</label>
                    <input type="file" class="form-control" id="profile_pic" name="picture" value="{{old('poster')}}">
                </div>
                <div class="form-group">
                    <label for="user_description">Description</label>
                    <textarea class="form-control" id="user_description" name="description" rows="8">{{old('description')}}</textarea>
                </div>

                <div class="row">
                    <div class="col-6">
                        <div class="d-flex justify-content-start">
                            <button class="btn btn-light" type="submit">Add profile data</button>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="d-flex justify-content-end">
                            <a class="btn btn-primary" href="{{url('/home/')}}">Skip</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection
