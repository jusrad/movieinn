@extends('layouts.app')

@section('style')
    .img-size{
    height: 300px;
    width: 600px;
    }

    .spacing{
    height: 46px;
    }
@endsection

@section('content')
    <section>
        <div class="text-white container-fluid">
            <h1 style="text-align: center">Edit profile</h1>
            <form method="post" enctype="multipart/form-data" action="{{url('/home/editForm/editUser')}}">
                @csrf
                <div style="width: 50%" class="form-group">
                    <label for="user_name">Username</label>
                    <input type="text" class="form-control" id="user_name" name="name" required value="{{ $user['name'] }}">
                </div>
                <div style="width: 20%" class="form-group">
                    <label for="profile_pic">User profile picture URL</label>
                    <input type="file" class="form-control" id="profile_pic" name="picture" value="{{ $user['avatar'] }}">
                </div>
                <div style="width: 30%" class="form-group">
                    <label for="user_tag">Tag</label>
                    <input type="text" class="form-control" id="user_tag" name="userTag" value="{{ $user['mark'] }}">
                </div>
                <div class="form-group">
                    <label for="user_description">Description</label>
                    <textarea class="form-control" id="user_description" name="description" rows="8">{{$user['description']}}</textarea>
                </div>

                <button class="btn btn-light" type="submit">Edit profile</button>
            </form>
        </div>
    </section>
@endsection
