@extends('layouts.app')

@section('content')
    <section>
        <div class="text-white container-fluid">
            <h1 style="text-align: center">Edit person data</h1>
            <form method="post" action="{{url('/people/details/edit', $person->id_Person)}}">
                @csrf
                <div class="form-row">
                    <div style="width: 100%" class="form-group">
                        <label for="firstName">First Name</label>
                        <input type="text" class="form-control" id="firstName" name="first_name" required value="{{$person->first_name}}">
                    </div>
                    <div style="width: 100%" class="form-group">
                        <label for="lastName">Last Name</label>
                        <input type="text" class="form-control" id="lastName" name="last_name" required value="{{$person->last_name}}">
                    </div>
                </div>
                <div style="width: 100%" class="form-group">
                    <label for="knowFor">Known for</label>
                    <select class="form-control" id="knowFor" name="known_for">
                        <option value="Acting" {{$person->known_for == 'Acting' ? 'selected' : '' }} >Acting</option>
                        <option value="Directing" {{$person->known_for == 'Directing' ? 'selected' : '' }}>Directing</option>
                        <option value="Writing" {{$person->known_for == 'Writing' ? 'selected' : '' }}>Writing</option>
                    </select>
                </div>
                <div class="form-row">
                    <div class="col">
                        <div class="form-group">
                            <label for="bday">Birthday</label><br>
                            <input style="width: 50%" type="date" id="bday" class="form-control" name="bday" required value="{{$person->birthday}}">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="bplace">From</label><br>
                            <input style="width: 100%" type="text" id="bplace" class="form-control" name="bplace" required value="{{$person->birth_place}}">
                        </div>
                    </div>
                </div>
                <div style="width: 50%" class="form-group">
                    <label for="imageURL">Picture URL</label>
                    <input type="url" class="form-control" id="imageURL" name="person-picture" value="{{$person->image}}">
                </div>
                <div class="form-group">
                    <label for="person_biography">Biography</label>
                    <textarea class="form-control" id="person_biography" name="biography" rows="20">{{$person->biography}}</textarea>
                </div>

                <button class="btn btn-light" type="submit">Edit person data</button>
            </form>
        </div>
    </section>
@endsection
