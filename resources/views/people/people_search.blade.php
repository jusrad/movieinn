@section('scripts')
    <script src="{{ asset('js/search_person.js') }}" defer></script>
@endsection

@section('css-file')
    <link rel=stylesheet href={{ asset('css/search.css') }}>
@endsection

<div class="search-container">
    <form id="film-search-form" autocomplete="off" method="get" action="{{url('/people/addPerson/auto')}}">
        <div style="display: inline-block" class="input-field">
            <input id="search-input" type="text" placeholder="Enter name of a person" name="person-name">
            <input id="search-selection-id" type="hidden" value="-1" name="person-id">
        </div>
        <button class="ml-2" type="submit">Add person</button>
    </form>
</div>
