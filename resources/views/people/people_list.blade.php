@extends('layouts.app')

@section('css-file')
    <link href="{{ asset('css/people_list.css') }}" rel="stylesheet">
@endsection

@section('content')
    <section class="text-white">
        <h1 class="centre-text">Popular people</h1>
        <div class="flex-container">
            @foreach($people as $person)
                <div id="person-{{$person['id_Person']}}" onclick="window.location='{{url("/people/details/". $person['id_Person'])}}'">
                    <div class="person-box">
                        <img alt="{{$person['first_name']}}_{{$person['last_name']}}-poster" class="person_image_size" src="{{$person['image']}}">
                        <h5 id="{{$person['first_name']}}_{{$person['last_name']}}-name" class="font-weight-bold">{{$person['first_name']}} {{$person['last_name']}}</h5>
                        <div id="{{$person['first_name']}}_{{$person['last_name']}}-known" >{{$person['known_for']}}</div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
@endsection
