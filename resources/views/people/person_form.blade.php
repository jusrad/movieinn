@extends('layouts.app')

@section('content')
    <section>
        <div class="text-white container-fluid">
            <h1 style="text-align: center">Add new person</h1>

            <h2>Add person from TMDb</h2>
            @include('people.people_search')
            <hr style="border-color: white">
            <h2>Add person data manually</h2>
            <form method="post" action="{{url('PersonForm/addPerson')}}">
                @csrf
                <div class="form-row">
                    <div class="col">
                        <div style="width: 50%" class="form-group">
                            <label for="idPerson">TMDb id (necessary)</label>
                            <input type="number" class="form-control" id="idPerson" name="id_Person" required value="{{ old('id_Person')}}">
                        </div>
                    </div>
                    <div class="col">
                        <div style="width: 50%" class="form-group">
                            <label for="knowFor">Known for</label>
                            <select class="form-control" id="knowFor" name="known_for">
                                <option value="Acting" >Acting</option>
                                <option value="Directing" >Directing</option>
                                <option value="Writing" >Writing</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <div class="form-group">
                            <label for="firstName">First Name</label>
                            <input style="width: 50%" type="text" class="form-control" id="firstName" name="first_name" required value="{{ old('first_name')}}">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="lastName">Last Name</label>
                            <input style="width: 50%" type="text" class="form-control" id="lastName" name="last_name" required value="{{ old('last_name')}}">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <div class="form-group">
                            <label for="bday">Birthday</label><br>
                            <input style="width: 50%" type="date" id="bday" class="form-control" name="bday" required value="{{ old('bday')}}">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="bplace">From</label><br>
                            <input style="width: 70%" type="text" class="form-control" id="bplace" name="bplace" required value="{{ old('bplace')}}">
                        </div>
                    </div>
                </div>
                <div style="width: 50%" class="form-group">
                    <label for="imageURL">Picture URL</label>
                    <input type="url" class="form-control" id="imageURL" name="person-picture" value="{{old('person-picture')}}">
                </div>
                <div class="form-group">
                    <label for="person_biography">Biography</label>
                    <textarea class="form-control" id="person_biography" name="biography" rows="20">{{old('biography')}}</textarea>
                </div>

                <button type="submit" class="btn btn-light">Add person</button>
            </form>
        </div>
    </section>
@endsection
