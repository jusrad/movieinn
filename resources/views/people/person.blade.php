@extends('layouts.app')

@section('css-file')
    <link href="{{ asset('css/movie.css') }}" rel="stylesheet">
@endsection

@section('content')
    <section>
        <div class="container-fluid text-white">
            <div class="row">
                <div class="col-12">
                    <div class="d-flex justify-content-center">
                        <h2 class="font-weight-bold text-white" id="film-name">{{$person['first_name']}} {{$person['last_name']}}</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    <img style="height: 70%" class="poster-size" id="img-display" alt="{{$person['first_name']}}_{{$person['last_name']}}_picture" src="{{$person['image']}}">
                </div>
                <div class="col-10">
                    <div>
                        Birthday: <b id="birthday">{{$person['birthday']}}</b><br>
                        From: <b id="birthplace">{{$person['birth_place']}}</b><br>
                        Known for: <b id="known_for">{{$person['known_for']}}</b><br>
                        <div class="text-newlines">Biography: {{ $person['biography'] }}</div>
                        <br>
                    </div>
                </div>
            </div>
            <br>
            <div class="row mt-3">
                <div class="col-10">
                    @if($isMarked)
                        <button class="btn btn-danger" onclick="window.location='{{url('/people/details/unmark/' . $person['id_Person'])}}'">
                            Don't get notifications
                        </button>
                    @else
                        <button class="btn btn-primary" onclick="window.location='{{url('/people/details/mark/' . $person['id_Person'])}}'">
                            Get notifications
                        </button>
                    @endif
                </div>
                <div class="col-2">
                    @guest
                    @else
                        @if(Auth::user()->hasRole('administratorius'))
                            <a class="btn btn-info ml-3"  href="{{url('/people/details/editForm/'. $person['id_Person'])}}">
                                <span>Edit</span>
                            </a>
                            <a class="btn btn-info" onclick="return confirm('Do you really want to delete this?')" href="{{url('/people/details/delete/'. $person['id_Person'])}}" >
                                <span>Delete</span>
                            </a>
                        @endif
                    @endguest
                </div>
            </div>
            <hr style="border-color: white">
            <h1 class="centre-text">Movie credits</h1>
            @if($credits === null)
                <hr style="border-color: white">
                <div style="text-align: center">
                    <b>No data</b>
                </div>
                <hr style="border-color: white">
            @else
                <table class="table table-dark">
                    <thead>
                    <tr>
                        <th scope="col">Release date</th>
                        <th scope="col">Title</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($credits as $credit)
                        <tr>
                            @if(array_key_exists('release_date', $credit))
                                <td>{{$credit['release_date']}}</td>
                            @else
                                <td>Unknown</td>
                            @endif
                            <td>{{$credit['title']}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </section>
@endsection
