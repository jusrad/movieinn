@foreach($comments as $comment)
    <div class="media-list mb-2" @if($comment->parent_id != null) style="margin-left:40px;" @endif>
        <div class="media">
            <a href="#" class="pull-left">
                <img src="https://static.thenounproject.com/png/363640-200.png" alt="" class="img-circle mb-1">
            </a>
            <div style="display:block" id="comment_display_{{$comment['id_TVshow_comment']}}" class="media-body">
                <span class="text-muted pull-right">
                    <small class="text-muted">{{$comment['create_date']}}</small>
                </span>

                <strong class="text-success">{{$comment['user']}}</strong>
                <p class="text-newlines">{{$comment['content']}}</p>
            </div>

            <form id="comment_edit_{{$comment['id_TVshow_comment']}}" style="display:none; width: 100%" method="post" action="{{route('editShowComment',$comment['id_TVshow_comment'])}}">
                <div class="row">
                    <div class="col-11">
                        <textarea wrap='hard' name="turinys" class="form-control" cols="100">{{$comment['content']}}</textarea>
                        {{ csrf_field() }}
                        <br>
                        <input type="hidden" name="parent_id" value="{{$comment['parent_id']}}" />
                    </div>
                    <div class="col-1">
                        <button name="film_id_post" value="{{$film}}" type="submit" class="btn btn-info pull-right">Post</button>
                    </div>
                </div>
            </form>
        </div>
        @guest
        @else
            <div class="ml-2" style="display: inline-block">
                <a onclick="document.getElementById('reply_' + {{ $comment->id_TVshow_comment }}).style.display='block'">Reply</a>
            </div>
            @if($comment['fk_com_User'] == Auth::user()->id or Auth::user()->hasRole('administratorius'))
                @if($comment['is_deleted'] == 0)
                    @if($comment['fk_com_User'] == Auth::user()->id)
                        <div class="ml-2"  style="display: inline-block">
                            <a onclick="displayEditForm('comment_edit_' + {{$comment['id_TVshow_comment']}})">Edit</a>
                        </div>
                    @endif
                    <div class="ml-2"  style="display: inline-block">
                        <a onclick="window.location='{{route('deleteShowComment',$comment['id_TVshow_comment'])}}'">Remove</a>
                    </div>
                @endif
            @endif
        @endguest
        <div id="reply_{{ $comment->id_TVshow_comment }}" style="display:none">
            <form class="mt-2" method="post" action="{{url('/home/show/postcom')}}">
                <textarea name="turinys" class="form-control" placeholder="write a comment..." rows="3"></textarea>
                {{ csrf_field() }}
                <br>
                <input type="hidden" name="parent_id" value="{{ $comment->id_TVshow_comment }}" />
                <button name="film_id_post" value="{{$film}}" type="submit" class="btn btn-info pull-right">Post</button>
            </form>
            <div class="clearfix"></div>
        </div>
        @include('comment.comment_display_tvshow', ['comments' => $comment->replies, 'film' => $film])
    </div>
@endforeach

<script>
    function displayEditForm(id){
        let display_id = id.replace('edit', 'display');
        document.getElementById(id).style.display = 'block';
        document.getElementById(display_id).style.display = 'none';
    }
</script>
