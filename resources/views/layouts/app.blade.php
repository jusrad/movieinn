<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/custom.js') }}" defer></script>
    @yield('scripts')

    <!-- Fonts -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('css-file')

    <style>
        .centre-text{
            text-align: center;
        }

        footer {
            padding-top: 1rem;
            padding-bottom: 1rem;
        }

        a{
            color: powderblue;
            text-decoration: none;
        }

        a:hover{
            color: #f2a365;
        }

        .dark-text{
            color: darkblue;
        }

        #wrap {
            min-height: 100%;
        }

        .flex-wrapper {
            display: flex;
            min-height: 100vh;
            flex-direction: column;
            justify-content: space-between
        }

        @yield('style')
    </style>
</head>
<body class="bg-dark flex-wrapper">
    <div id="wrap">
        <div id="app">
            <nav class="navbar navbar-expand-md navbar-dark bg-primary shadow-sm">
                <div class="container">
                    <a class="navbar-brand" href="{{ url('/home') }}">
                        <i class="d-inline-block fa fa-film fa-sm"></i>
                        {{ config('app.name', 'Laravel') }}
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/home/') /*route('MoviePage')*/}}">Movies</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/show/showList')}}">TV Shows</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('people/peopleList/')}}">People</a>
                            </li>
                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">Log in</a>
                                </li>
                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('register') }}">Register</a>
                                    </li>
                                @endif
                            @else
                                @if(Auth::user()->hasRole('administratorius'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('addFilmForm', 0)}}">Add movie</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('addFilmForm', 1)}}">Add TV show</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('addPersonForm')}}">Add person</a>
                                    </li>
                                @endif
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link active dropdown-toggle dark-text" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        @if(Auth::user()->hasRole('administratorius'))
                                            <a class="dropdown-item" href="{{ route('sendEmail') }}">Send email</a>
                                            <a class="dropdown-item" href="{{ route('getReleases') }}">Update Releases</a>
                                            <a class="dropdown-item" href="{{ route('userlist') }}">User list</a>
                                            <hr>
                                        @endif
                                        <a class="dropdown-item" href="{{ url('/home/profile') }}">Profile</a>
                                        <hr>
                                        <a class="dropdown-item" href="{{ url('/home/markedPeople') }}">Marked people</a>
                                        <a class="dropdown-item" href="{{ url('/home/lists') }}">Created lists</a>
                                        <a class="dropdown-item" href="{{ url('/home/watchlist/') }}">Watchlist</a>
                                        <hr>
                                        <a class="dropdown-item" href="{{ route('edituser') }}">Edit profile</a>
                                        <a class="dropdown-item" href="{{ route('password.request') }}">Reset password</a>
                                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>

            <main class="py-4">
                @yield('content')
            </main>

            <div id="error_display" class="container-fluid">
                <div class="row">
                    <div class="col">
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <p style="margin: 0; text-align: center">{!! \Session::get('success') !!}</p>
                            </div>
                        @endif
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <p style="margin: 0; text-align: center">{{ $error }}</p>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        window.setTimeout(closeHelpDiv, 5000);

        function closeHelpDiv(){
            document.getElementById("error_display").style.display="none";
        }
    </script>

    @yield('search')
    <!-- Footer -->
    <footer class="page-footer text-light bg-primary">
        <div class="container">
            <div class="row row-cols-4">
                <div class="col">
                    <p>
                        <a class="text-white" href="{{route('aboutPage')}}">About</a>
                    </p>
                </div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
            </div>
            <div class="row">
                <!-- Copyright -->
                <div class="container text-center text-white py-2">
                    This product uses the <a href="https://www.themoviedb.org/">TMDb</a> API but is not endorsed or certified by TMDb.
                </div>
            </div>
        </div>
    </footer>
</body>
</html>
