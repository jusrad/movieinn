@section('scripts')
    <script src="{{ asset('js/search_tvshow.js') }}" defer></script>
@endsection

@section('css-file')
    <link rel=stylesheet href={{ asset('css/search.css') }}>
@endsection

<div class="search-container">
    <form id="film-search-form" autocomplete="off" method="get" action="{{url('/show/addShow/auto')}}">
        <div style="display: inline-block" class="input-field">
            <input id="search-input" type="text" placeholder="Enter TV show name" name="tvshow-name">
            <input id="search-selection-id" type="hidden" value="-1" name="tvshow-id">
        </div>
        <button class="ml-2" type="submit">Add TV show</button>
    </form>
</div>
