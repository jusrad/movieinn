@extends('layouts.app')

@section('content')
    <section>
        <div class="text-white container-fluid">
            <h1 style="text-align: center">Add a new TV show</h1>

            <h2>Add TV show by name from TMDb</h2>
            @include('TVshow.TVshow_search')
            <hr style="border-color: white">
            <h2>Add TV show manually</h2>
            <form method="post" action="{{url('FilmForm/addTVShow')}}">
                @csrf
                <div style="width: 20%" class="form-group">
                    <label for="idTVshow">TMDb id</label>
                    <input type="number" class="form-control" id="idTVshow" name="id_tvshow" required value="{{ old('id_tvshow')}}">
                </div>
                <div style="width: 50%" class="form-group">
                    <label for="title_tvshow">Name</label>
                    <input type="text" class="form-control" id="title_tvshow" name="title" required value="{{ old('title')}}">
                </div>
                <div class="form-row">
                    <div class="col">
                        <div class="form-group">
                            <label for="rdate">Release date</label><br>
                            <input style="width: 50%" type="date" id="rdate" name="rdate" required value="{{ old('rdate')}}">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="last_rdate">Last episode release date</label><br>
                            <input style="width: 50%" type="date" id="last_rdate" name="Lastrdate" required value="{{ old('Lastrdate')}}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tvshow_type">Type</label>
                    <input style="width: 30%" type="text" class="form-control" id="tvshow_type" name="type" value="{{ old('type')}}">
                </div>
                <div class="form-group">
                    <label for="director_list_string">Creators (separate by comma)</label>
                    <input type="text" class="form-control" id="director_list_string" name="directors" value="{{ old('directors')}}">
                </div>
                <div class="form-group">
                    <label for="actor_list_string">Actors (separate by comma)</label>
                    <input type="text" class="form-control" id="actor_list_string" name="actors" value="{{ old('actors')}}">
                </div>
                <div class="form-group">
                    <label for="writer_list_string">Writers (separate by comma)</label>
                    <input type="text" class="form-control" id="writer_list_string" name="writers" value="{{ old('writers')}}">
                </div>
                <div class="form-row">
                    <div class="col">
                        <div style="width: 50%" class="form-group">
                            <label for="movie_genres">Select genres</label>
                            <select style="height: 180px"  multiple class="form-control" id="movie_genres" name="genres[]">
                                @foreach($allGenre as $key => $genre)
                                    <option value="{{$genre['id_Genre']}}" {{collect(old('genres'))->contains($genre['id_Genre']) ? 'selected' : ''}}>{{$genre['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div style="width: 50%" class="form-group">
                            <label for="movie_rating">TV show rating</label>
                            <input type="number" step="0.1" class="form-control" id="movie_rating" name="rating" value="{{ old('rating')}}">
                        </div>
                    </div>
                </div>
                <div style="width: 30%" class="form-group">
                    <label for="season_nr">Number of seasons</label>
                    <input type="number" class="form-control" id="season_nr" name="SeasonNr" value="{{old('SeasonNr')}}">
                </div>
                <div style="width: 50%" class="form-group">
                    <label for="posterURL">TV show poster URL</label>
                    <input type="url" class="form-control" id="posterURL" name="poster" value="{{old('poster')}}">
                </div>
                <div class="form-group">
                    <label for="movie_summ">Summary</label>
                    <textarea class="form-control" id="movie_summ" name="summary" rows="15">{{old('summary')}}</textarea>
                </div>

                <button class="btn btn-light" type="submit">Add tv show</button>
            </form>
        </div>
    </section>
@endsection
