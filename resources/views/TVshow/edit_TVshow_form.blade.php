@extends('layouts.app')

@section('content')
    <section>
        <div class="text-white container-fluid">
            <h1 style="text-align: center">Edit TV show</h1>
            <form method="post" action="{{route('editShow', $tvshow->id_TVShow)}}">
                @csrf
                <div style="width: 50%" class="form-group">
                    <label for="title_tvshow">Name</label>
                    <input type="text" class="form-control" id="title_tvshow" name="title" required value="{{ $tvshow->title }}">
                </div>
                <div class="form-row">
                    <div class="col">
                        <div class="form-group">
                            <label for="rdate">Release date</label><br>
                            <input style="width: 50%" type="date" id="rdate" name="rdate" required value="{{ $tvshow->release_date }}">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="last_rdate">Last episode release date</label><br>
                            <input style="width: 50%" type="date" id="last_rdate" name="Lastrdate" required value="{{ $tvshow->last_air_date }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tvshow_type">Type</label>
                    <input style="width: 30%" type="text" class="form-control" id="tvshow_type" name="type" value="{{ $tvshow->type }}">
                </div>
                <div class="form-group">
                    <label for="director_list_string">Creators (separate by comma)</label>
                    <input type="text" class="form-control" id="director_list_string" name="directors" value="{{ $tvshow->director_list }}">
                </div>
                <div class="form-group">
                    <label for="actor_list_string">Actors (separate by comma)</label>
                    <input type="text" class="form-control" id="actor_list_string" name="actors" value="{{ $tvshow->actor_string }}">
                </div>
                <div class="form-group">
                    <label for="writer_list_string">Writers (separate by comma)</label>
                    <input type="text" class="form-control" id="writer_list_string" name="writers" value="{{ $tvshow->writer_string }}">
                </div>
                <div class="form-row">
                    <div class="col">
                        <div style="width: 50%" class="form-group">
                            <label for="movie_genres">Select genres</label>
                            <select style="height: 180px"  multiple class="form-control" id="movie_genres" name="genres[]">
                                @foreach($allGenre as $key => $genre)
                                    <option value="{{$genre['id_Genre']}}" {{$tvshow_genres->contains('id_Genre', $genre->id_Genre) ? 'selected' : ''}}>{{$genre['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div style="width: 50%" class="form-group">
                            <label for="movie_rating">TV show rating</label>
                            <input type="number" step="0.1" class="form-control" id="movie_rating" name="rating" value="{{ $tvshow->rating }}">
                        </div>
                    </div>
                </div>
                <div style="width: 30%" class="form-group">
                    <label for="season_nr">Number of seasons</label>
                    <input type="number" class="form-control" id="season_nr" name="SeasonNr" value="{{ $tvshow->season_number }}">
                </div>
                <div style="width: 50%" class="form-group">
                    <label for="posterURL">TV show poster URL</label>
                    <input type="url" class="form-control" id="posterURL" name="poster" value="{{ $tvshow->posterURL }}">
                </div>
                <div class="form-group">
                    <label for="movie_summ">Summary</label>
                    <textarea class="form-control" id="movie_summ" name="summary" rows="15">{{ $tvshow->overview }}</textarea>
                </div>
                <div class="form-group">
                    <label>Do you want ot reset user vote count?</label>
                    <br>
                    <input type="radio" id="reset_yes" name="reset_check" value="true">
                    <label for="reset_yes">Yes</label>
                    <br>
                    <input type="radio" id="reset_no" name="reset_check" value="false" checked>
                    <label for="reset_no">No</label>
                </div>
                <button class="btn btn-light" type="submit">Edit TV show</button>
            </form>
        </div>
    </section>
@endsection
