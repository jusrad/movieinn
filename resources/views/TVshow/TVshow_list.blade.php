@extends('layouts.app')

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
@endsection

@section('css-file')
    <link href="{{ asset('css/movie_list.css') }}" rel="stylesheet">
@endsection

@section('content')
    <section>
        <div style="margin: 0 12px">
            <h1 class="centre-text text-white">Popular TV shows</h1>
            <div class="container-fluid">
                @foreach($allFilms as $chunk)
                    <div class="row my-2">
                        @foreach($chunk as $film)
                            <div id="{{$film['id_TVShow']}}" @if($film['id_TVShow'] === null) style="visibility: hidden;" @endif class="col-md col-sm bg-secondary mx-1" onclick="window.location='{{url("/show/details/". $film['id_TVShow'])}}'">
                                <div class="row">
                                    <div class="col-md-6 col-sm p-0">
                                        <img alt="{{$film['title']}}-poster" class="poster-size" src="{{$film['posterURL']}}">
                                    </div>
                                    <div class="col-md-6 col-sm py-2 centre-text">
                                        <div style="height: 20%" class="d-flex flex-row align-items-start justify-content-center">
                                            <div>
                                                <h5 id="name_{{$film['id_TVShow']}}" class="font-weight-bold text-white">{{$film['title']}}</h5>
                                            </div>
                                        </div>
                                        <div style="height: 80%" class="d-flex align-items-end justify-content-center">
                                            <div class="mt-auto">
                                                <div class="IMDB-rating">TMDb {{$film['rating']}}</div>
                                                <div class="text-white">{{$film['release_date']}}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <script>

    </script>
@endsection
