@extends('layouts.app')

@section('css-file')
    <link href="{{ asset('css/movie.css') }}" rel="stylesheet">
@endsection

@section('content')
    <section>
        <div class="container-fluid text-white">
            <div class="row">
                <div class="col-12">
                    <div class="d-flex justify-content-center">
                        <h2 class="font-weight-bold text-white" id="film-name">{{$tvshow->title}}</h2>
                    </div>
                </div>
            </div>
            <div class="row my-2">
                <div class="col-2">
                    <img class="poster-size" id="img-display" alt="{{$tvshow->title}}-poster" src="{{$tvshow->posterURL}}">
                </div>
                <div class="col">
                    <div>
                        Release date: <b id="year">{{$tvshow->release_date}}</b><br>
                        Last episode release date: <b id="last_year">{{$tvshow->last_air_date}}</b><br>
                        TMDb rating: <b id="rating">{{$tvshow->rating}}</b><br>
                        Number of seasons: <b id="season_nr">{{$tvshow->season_number}}</b><br>
                        Genres:
                        @foreach($genres as $genre)
                            @if($loop->last)
                                <b>{{$genre['name']}}</b>
                            @else
                                <b>{{$genre['name']}}, </b>
                            @endif
                        @endforeach
                        <br>
                        Creators: {{$tvshow->director_list}}
                        <br>
                        Writers: @if($tvshow->writer_string === '0') - @else {{$tvshow->writer_string}} @endif
                        <br>
                        Actors: {{$tvshow->actor_string}}
                        <br>
                    </div>
                    <div class="mt-2">
                        <a class="like bg-success" href="{{route('like_dislike_tvshow', [$tvshow->id_TVShow, 1])}}">
                            <i class="fa fa-thumbs-o-up"></i>
                            Like
                        </a>
                        <input class="counter text-white" name="counter" readonly="readonly" type="text" value="{{$tvshow->user_rating}}" />
                        <a class="like bg-danger" href="{{route('like_dislike_tvshow', [$tvshow->id_TVShow, -1])}}">
                            <i class="fa fa-thumbs-o-down"></i>
                            Dislike
                        </a>
                    </div>
                </div>
            </div>
            <div class="row my-2">
                <div class="col">
                    Summary:<p id="summary">{{$tvshow->overview}}</p><br>
                </div>
            </div>

            <div class="row my-2">
                <div class="col-10">
                    <button class="btn btn-light" onclick="window.location='{{url('/home/addToList/'.$tvshow->id_TVShow.'/'.'2')}}'">Add to list</button>
                    @if($isInWatchlist)
                        <button class="btn btn-danger" onclick="window.location='{{url('/home/removeWatchlist/'.$tvshow->id_TVShow.'/'.'1')}}'">Remove from watchlist</button>
                    @else
                        <button class="btn btn-primary" onclick="window.location='{{url('/home/addWatchlist/'.$tvshow->id_TVShow.'/'.'1')}}'">Add to watchlist</button>
                    @endif
                </div>
                <div class="col-2">
                    @guest
                    @else
                        @if(Auth::user()->hasRole('administratorius'))
                            <a class="btn btn-info"  href="{{route('editShowForm', $tvshow->id_TVShow)}}">
                                <span>Edit</span>
                            </a>
                            <a class="btn btn-info" onclick="return confirm('Do you really want to delete this?')" href="{{route('deletetvshow',$tvshow->id_TVShow)}}" >
                                <span>Delete</span>
                            </a>
                        @endif
                    @endguest
                </div>
            </div>
            <hr style="border-color: white">
            <div class="row mt-2 text-white">
                <div style="margin:auto" class="col-md-10">
                    <div class="comment-wrapper">
                        <div class="panel panel-info">
                            <h3 class="mb-1">Comment</h3>
                            <div class="panel-body">
                                @guest
                                    <form method="post" action="{{url('/home/show/postcom')}}">
                                        <textarea wrap='hard' name="turinys" class="form-control" placeholder="write a comment..." rows="5" cols="100"></textarea>
                                        {{ csrf_field() }}
                                        <br>
                                    </form>
                                    <div class="clearfix"></div>
                                    <hr>
                                @else
                                    <form method="post" action="{{url('/home/show/postcom')}}">
                                        <textarea wrap='hard' name="turinys" class="form-control" placeholder="write a comment..." rows="5" cols="100"></textarea>
                                        {{ csrf_field() }}
                                        <br>
                                        <input type="hidden" name="parent_id" value="0" />
                                        <button name="film_id_post" value="{{$tvshow->id_TVShow}}" type="submit" class="btn btn-info pull-right">Post</button>
                                    </form>
                                    <div class="clearfix"></div>
                                    <hr>
                                @endguest
                                @include('comment.comment_display_tvshow', ['comments' => $comments, 'film' => $tvshow->id_TVShow])
                                {{ $comments->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
