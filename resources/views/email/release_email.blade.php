Dear {{$username}},
<br>
Here are the upcoming movies and TV shows from people you like!
<br>
<br>
@foreach($people as $person)
    <b>{{$person['first_name']}} {{$person['last_name']}}</b>
    <br>
    <table style="width:100%; font-family: arial, sans-serif; border-collapse: collapse;">
        <tr>
            <th>Name</th>
            <th>Release date</th>
            <th>Type</th>
        </tr>
        @foreach($person->releases as $release)
            <tr>
                <td>{{$release['name']}}</td>
                <td>@if($release['release_date'] === null) - @else {{$release['release_date']}} @endif</td>
                <td>{{$release['type']}}</td>
            </tr>
        @endforeach
    </table>
@endforeach
